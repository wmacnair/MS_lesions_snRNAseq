---
title: "ANCOM-BC bootstrapped to handle random effects"
author:
- name: Will Macnair
  affiliation: 
  - Neurogenomics, Neuroscience and Rare Diseases, Roche
date: '`r format(Sys.Date(), "%B %d, %Y")`'
output:
  workflowr::wflow_html:
    code_folding: hide
    toc: true
    toc_float: true
    number_sections: false
---

# Setup / definitions

## Libraries

```{r setup_knitr, include=FALSE}
library('BiocStyle')
knitr::opts_chunk$set( autodep=TRUE, cache=TRUE, cache.lazy=FALSE, dev='png' )
knitr::opts_knit$set( root.dir='..' )
# workflowr::wflow_build(files='analysis/ms09_ancombc_mixed.Rmd', view=F, verbose=T, delete_cache=F)
```

```{r setup_libs, collapse=FALSE, message=FALSE, warning=FALSE, cache=FALSE}
```

## Helper functions

```{r setup_helpers, message=FALSE, cache=FALSE}
source('code/ms00_utils.R')
source('code/ms04_conos.R')
source('code/ms07_soup.R')
source('code/ms09_ancombc_mixed.R')
setDTthreads(8)
```

## Inputs

```{r setup_input}
# define run
labels_f    = 'data/byhand_markers/validation_markers_2021-05-31.csv'
labelled_f  = 'output/ms13_labelling/conos_labelled_2021-05-31.txt.gz'
meta_f      = "data/metadata/metadata_checked_assumptions_2022-03-22.xlsx"

# define pseudobulk data
soup_dir    = 'output/ms07_soup'
pb_broad_f  = file.path(soup_dir, 'pb_sum_broad_2021-10-11.rds')
pb_fine_f   = file.path(soup_dir, 'pb_sum_fine_2021-10-11.rds')
pb_f_ls     = c(broad = pb_broad_f, fine = pb_fine_f)

# celltype proportions data
prop_fine_f = file.path(soup_dir, 'pb_prop_fine_2021-10-11.rds')

# GPR17 IHC staining data
gpr17_f     = 'data/gpr17_ihc/gpr17_ihc_data_2022-02-16.csv'
```

## Outputs

```{r setup_outputs}
# where to save?
save_dir    = 'output/ms09_ancombc'
date_tag    = '2021-11-12'
if (!dir.exists(save_dir))
    dir.create(save_dir)

# sample variables
sample_vars = c('sample_id', 'matter', 'lesion_type', 
  'neuro_ok', 'neuro_prop', 'sample_source', 'subject_id', 
  'sex', 'age_scale', 'pmi_cat', 'pmi_cat2')

# identifying strange samples
neuro_mad_cut = 2
log_n_mad_cut = 3

# define how to select PCs
cut_var_exp   = 0.01
cut_layer_cor = 0.2

# define WM data
wm_spec     = list(
  name        = 'lesions_WM',
  subset      = list(matter = 'WM', neuro_ok = TRUE),
  size        = list(min_count = 10, min_prop = 0.1),
  exc_regex   = '^(Ex_|Inh_|Neuro_oligo)',
  formula     = '~ lesion_type + sex + age_scale + pmi_cat',
  fixef_test  = 'lesion_type',
  fixef_covar = c('sex', 'age_scale', 'pmi_cat'),
  ranef_var   = 'subject_id'
)
gm_spec     = list(
  name        = 'lesions_GM',
  subset      = list(matter = 'GM', neuro_ok = TRUE),
  size        = list(min_count = 10, min_prop = 0.1),
  exc_regex   = NULL,
  formula     = '~ lesion_type + sex + age_scale + pmi_cat2',
  fixef_test  = 'lesion_type',
  fixef_covar = c('sex', 'age_scale', 'pmi_cat2'),
  ranef_var   = 'subject_id'
)

# define multiple different ways to do subspaces
gm_pc_spec  = list(
  name_str    = 'lesions_GM_',
  subset      = list(matter = 'GM', neuro_ok = TRUE),
  size        = list(min_count = 10, min_prop = 0.1),
  exc_regex   = NULL,
  formula_pat = '~ lesion_type + %s + sex + age_scale + pmi_cat2',
  fixef_test  = 'lesion_type',
  fixef_covar = c('sex', 'age_scale', 'pmi_cat2'),
  ranef_var   = 'subject_id',
  broad_sel   = c("Excitatory neurons", "Inhibitory neurons"),
  lesion_ctrl = "GM",
  n_pcs       = NA
)

# define multiple different ways to do subspaces
nagm_pc_spec  = list(
  name_str    = 'lesions_NAGM_',
  subset      = list(matter = 'GM', neuro_ok = TRUE),
  size        = list(min_count = 10, min_prop = 0.1),
  exc_regex   = NULL,
  formula_pat = '~ lesion_type + %s + sex + age_scale + pmi_cat2',
  fixef_test  = 'lesion_type',
  fixef_covar = c('sex', 'age_scale', 'pmi_cat2'),
  ranef_var   = 'subject_id',
  broad_sel   = c("Excitatory neurons", "Inhibitory neurons"),
  lesion_ctrl = "GM",
  n_pcs       = NA
)

# gather things
spec_list         = list(wm_spec, gm_spec)
names(spec_list)  = sapply(spec_list, function(l) l$name)

# bootstrapping parameters
n_boots     = 2e4
n_cores     = 4

# define files for saving outputs
lrt_pat     = file.path(save_dir, 'abundance_nb_lrt_model_%s_%s.rds')
clr_pat     = file.path(save_dir, 'clr_clustering_%s_%s.txt')
ancom_pat   = file.path(save_dir, 'ancombc_standard_%s_%s.rds')
boots_pat   = file.path(save_dir, 'ancombc_bootstrap_%s_%s.txt.gz')

# define files for saving outputs
pb_pcs_ls   = c(
  broad = sprintf('%s/pb_gm_w_pcs_sum_broad_%s.rds', soup_dir, date_tag),
  fine  = sprintf('%s/pb_gm_w_pcs_sum_fine_%s.rds', soup_dir, date_tag)
  )

# define various groups of celltypes
olg_types   = c('OPCs / COPs', 'Oligodendrocytes')
neu_types   = c('Excitatory neurons', 'Inhibitory neurons')
wm_types    = setdiff(broad_ord, c('Excitatory neurons', 'Inhibitory neurons'))
gm_types    = setdiff(broad_ord, c('Immune'))

# define parameters for CLR plots
min_cells   = 50
olg_grps_f  = 'data/metadata/oligo_groupings.txt'
olg_clust_f = sprintf("%s/oligo_clr_clusters_%s.txt", save_dir, date_tag)

# params for plotting GPR17 cell abundances
sel_g       = "GPR17"
```

# Load inputs

```{r load_conos, cache = FALSE}
meta_dt     = load_meta_dt_from_xls(meta_f)
labels_dt   = load_names_dt(labels_f) %>%
  .[, cluster_id := type_fine]
conos_all   = load_labelled_dt(labelled_f, labels_f) %>%
  merge(meta_dt, by = 'sample_id') %>%
  add_neuro_props(mad_cut = neuro_mad_cut)
```

```{r remove_weird_samples, cache = FALSE}
# check for any outliers
size_chks   = calc_size_outliers(conos_all, mad_cut = log_n_mad_cut)
message("these samples excluded to outlier sample sizes:")
print(size_chks[ size_ok == FALSE ])

# exclude them from conos
ok_samples  = size_chks[ size_ok == TRUE ]$sample_id
conos_all   = conos_all[ sample_id %in% ok_samples ]
conos_dt    = conos_all[ (sample_id %in% ok_samples) & (neuro_ok == TRUE) ]
```

```{r calc_wide_dt}
props_dt    = calc_props_dt(conos_dt, sample_vars)
wide_dt     = calc_counts_wide(props_dt, sample_vars)
```

```{r calc_gm_layer_pcs}
# get neuronal proportions for all samples
props_neu   = conos_dt %>%
  .[ (type_broad %in% gm_pc_spec$broad_sel) ] %>% 
  calc_props_dt(sample_vars)

# calc PCAs
ctrl_pcs_dt = props_neu %>% 
  .[ lesion_type == gm_pc_spec$lesion_ctrl ] %>%
  calc_ctrl_pcs_dt(layers_dt)
```

```{r calc_gm_wide_pcs}
# apply pcs
all_pcs_dt  = apply_ctrl_pcs(props_neu[ matter == "GM" ], ctrl_pcs_dt, 
  cut_var_exp, cut_layer_cor)
wide_neu    = merge(all_pcs_dt, wide_dt, 'sample_id')
pc_vars     = str_subset(names(wide_neu), "ctrl_PC")
```

```{r load_gpr17_ihc_data}
gpr17_dt    = load_gpr17_dt(gpr17_f)
```

# Processing / calculations

```{r calc_lrts_std}
# negative-binomial model on unadjusted counts, WM
nb_wm_f     = sprintf(lrt_pat, wm_spec$name, date_tag)
if (file.exists(nb_wm_f)) {
  nb_wm_ls    = readRDS(nb_wm_f)
} else {
  nb_wm_ls    = calc_celltype_mixed_models(wide_dt, sample_vars,
    wm_spec$subset, wm_spec$size, wm_spec$exc_regex, wm_spec$inc_regex,
    wm_spec$fixef_test, wm_spec$fixef_covar, wm_spec$ranef_var, 
    n_cores = n_cores, offset_var = NULL)
  saveRDS(nb_wm_ls, file = nb_wm_f)  
}

# negative-binomial model on unadjusted counts, GM
nb_gm_f     = sprintf(lrt_pat, gm_spec$name, date_tag)
if (file.exists(nb_gm_f)) {
  nb_gm_ls    = readRDS(nb_gm_f)
} else {
  nb_gm_ls    = calc_celltype_mixed_models(wide_dt, sample_vars,
    gm_spec$subset, gm_spec$size, gm_spec$exc_regex, gm_spec$inc_regex,
    gm_spec$fixef_test, gm_spec$fixef_covar, gm_spec$ranef_var, 
    n_cores = n_cores, offset_var = NULL)
  saveRDS(nb_gm_ls, file = nb_gm_f)  
}
```

```{r calc_lrts_pcs}
# make spec
for (n_pcs in seq_along(pc_vars)) {
  # which PCs?
  layer_spec  = make_layer_pc_spec(gm_pc_spec, pc_vars, n_pcs = n_pcs)


  # negative-binomial model on unadjusted counts, GM, w layers
  nb_layers_f = sprintf(lrt_pat, layer_spec$name, date_tag)
  if (!file.exists(nb_layers_f)) {
    nb_layers_ls  = calc_celltype_mixed_models(wide_neu, c(sample_vars, pc_vars),
      layer_spec$subset, layer_spec$size, layer_spec$exc_regex, layer_spec$inc_regex,
      layer_spec$fixef_test, layer_spec$fixef_covar, layer_spec$ranef_var, 
      n_cores = n_cores, offset_var = NULL)
    saveRDS(nb_layers_ls, file = nb_layers_f)  
  }
}

# load them
nb_pcs_ls   = lapply(seq_along(pc_vars), function(n_pcs) {
  # make file
  layer_spec  = make_layer_pc_spec(gm_pc_spec, pc_vars, n_pcs = n_pcs)
  nb_gm_ls    = sprintf(lrt_pat, layer_spec$name, date_tag) %>%
    readRDS
  
  return(nb_gm_ls)
  }) %>% setNames(paste0(gm_pc_spec$name_str, seq_along(pc_vars), 'pcs'))

# make list of models
lrt_ls      = list(WM = nb_wm_ls, GM = nb_gm_ls) %>%
  c(nb_pcs_ls)
```

```{r calc_ancom_standard}
# loop through specified models
for (nn in names(spec_list)) {
  # make file
  ancom_f   = sprintf(ancom_pat, spec_list[[nn]]$name, date_tag)
  
  # if necessary, run thing
  if (!file.exists(ancom_f)) {
    message('running standard ANCOM-BC for ', nn)
    # define things we need
    spec      = spec_list[[nn]]

    # do standard ANCOM, save results
    ancom_obj = calc_ancom_standard(wide_dt, sample_vars,
      spec$subset, spec$size, spec$exc_regex, spec$inc_regex, spec$ref_type,
      spec$fixef_test, spec$fixef_covar)
    saveRDS(ancom_obj, file = ancom_f)
  }
}
```

```{r calc_ancom_bootstraps}
# loop through specified models
for (nn in names(spec_list)) {
  # make file
  boots_f   = sprintf(boots_pat, spec_list[[nn]]$name, date_tag)
  
  # if necessary, run thing
  if (!file.exists(boots_f)) {
    message('running bootstrapped ANCOM-BC for ', nn)
    # define things we need
    spec      = spec_list[[nn]]

    # do bootstrapping, save resulst
    boots_dt  = calc_ancom_bootstrap(wide_dt, sample_vars,
      spec$subset, spec$size, spec$exc_regex, spec$inc_regex, spec$ref_type,
      spec$fixef_test, spec$fixef_covar, spec$ranef_var, 
      seed = 1, n_boots, n_cores)
    fwrite(boots_dt, file = boots_f)
  }
}
```

```{r calc_ancom_neuron_subspaces}
# set up this run
for (n_pcs in seq_along(pc_vars)) {
  # which PCs?
  layer_spec  = make_layer_pc_spec(gm_pc_spec, pc_vars, n_pcs = n_pcs)

  # make file
  ancom_f   = sprintf(ancom_pat, layer_spec$name, date_tag)

  # if necessary, run thing
  if (file.exists(ancom_f)) {
    message('standard ANCOM-BC for ', layer_spec$name, ' already done')
  } else {
    # do bootstrapping, save results
    message('running standard ANCOM-BC for ', layer_spec$name)
    ancom_neu = calc_ancom_standard(wide_neu, c(sample_vars, pc_vars),
      layer_spec$subset, layer_spec$size, layer_spec$exc_regex, 
      layer_spec$inc_regex, layer_spec$ref_type,
      layer_spec$fixef_test, layer_spec$fixef_covar)
    saveRDS(ancom_neu, file = ancom_f)
  }

  # do bootstrapping
  boots_f   = sprintf(boots_pat, layer_spec$name, date_tag)

  # if necessary, run thing
  if (file.exists(boots_f)) {
    message('bootstrapped ANCOM-BC for ', layer_spec$name, ' already done')
  } else {
    # do bootstrapping, save resulst
    message('running bootstrapped ANCOM-BC for ', layer_spec$name)
    t_start    = Sys.time()
    boots_neu = calc_ancom_bootstrap(wide_neu, c(sample_vars, pc_vars),
      layer_spec$subset, layer_spec$size, layer_spec$exc_regex, 
      layer_spec$inc_regex, layer_spec$ref_type,
      layer_spec$fixef_test, layer_spec$fixef_covar, layer_spec$ranef_var, 
      seed = 1, n_boots, n_cores)
    t_stop    = Sys.time()
    fwrite(boots_neu, file = boots_f)

    # report how long it took
    t_elapsed = difftime(t_stop, t_start, units = 'mins') %>% unclass
    message(sprintf(
      paste0('  (bootstrapping %d boots with %d cores took %.1f minutes;',
        ' %.1f boots / min / core)'), 
      n_boots, n_cores, t_elapsed, n_boots / t_elapsed / n_cores))
  }
}
```

```{r calc_ancom_neuron_nagm}
# set up this run
nagm_pcs    = 4
layer_spec  = make_layer_pc_spec(nagm_pc_spec, pc_vars, n_pcs = nagm_pcs)
wide_nagm   = wide_neu %>% copy %>% 
  .[, lesion_type := lesion_type %>% fct_relevel("NAGM") %>% fct_drop ]

# make file
ancom_f     = sprintf(ancom_pat, layer_spec$name, date_tag)


# if necessary, run thing
if (file.exists(ancom_f)) {
  message('standard ANCOM-BC for ', layer_spec$name, ' already done')
  ancom_nagm  = ancom_f %>% readRDS
} else {
  # do bootstrapping, save results
  message('running standard ANCOM-BC for ', layer_spec$name)
  ancom_nagm  = calc_ancom_standard(wide_nagm, c(sample_vars, pc_vars),
    layer_spec$subset, layer_spec$size, layer_spec$exc_regex, 
    layer_spec$inc_regex, layer_spec$ref_type,
    layer_spec$fixef_test, layer_spec$fixef_covar)
  saveRDS(ancom_nagm, file = ancom_f)
}

# do bootstrapping
boots_f   = sprintf(boots_pat, layer_spec$name, date_tag)

# if necessary, run thing
if (file.exists(boots_f)) {
  message('bootstrapped ANCOM-BC for ', layer_spec$name, ' already done')
  boots_nagm  = boots_f %>% fread
} else {
  # do bootstrapping, save resulst
  message('running bootstrapped ANCOM-BC for ', layer_spec$name)
  t_start    = Sys.time()
  boots_nagm = calc_ancom_bootstrap(wide_nagm, c(sample_vars, pc_vars),
    layer_spec$subset, layer_spec$size, layer_spec$exc_regex, 
    layer_spec$inc_regex, layer_spec$ref_type,
    layer_spec$fixef_test, layer_spec$fixef_covar, layer_spec$ranef_var, 
    seed = 1, n_boots, n_cores)
  t_stop    = Sys.time()
  fwrite(boots_nagm, file = boots_f)

  # report how long it took
  t_elapsed = difftime(t_stop, t_start, units = 'mins') %>% unclass
  message(sprintf(
    paste0('  (bootstrapping %d boots with %d cores took %.1f minutes;',
      ' %.1f boots / min / core)'), 
    n_boots, n_cores, t_elapsed, n_boots / t_elapsed / n_cores))
}
```

```{r load_ancom_standard}
ancom_ls  = lapply(names(spec_list), function(nn) {
  # make file
  ancom_obj  = sprintf(ancom_pat, spec_list[[nn]]$name, date_tag) %>% 
    readRDS
  
  return(ancom_obj)
  }) %>% setNames(names(spec_list))
```

```{r load_ancom_bootstraps}
boots_ls  = lapply(names(spec_list), function(nn) {
  # make file
  boots_dt  = sprintf(boots_pat, spec_list[[nn]]$name, date_tag) %>% fread
  
  return(boots_dt)
  }) %>% setNames(names(spec_list))
```

```{r load_ancom_neuron_subspaces}
# load std
ancom_pcs_ls  = lapply(seq_along(pc_vars), function(n_pcs) {
  # make file
  layer_spec  = make_layer_pc_spec(gm_pc_spec, pc_vars, n_pcs = n_pcs)
  ancom_obj   = sprintf(ancom_pat, layer_spec$name, date_tag) %>%
    readRDS
  
  return(ancom_obj)
  }) %>% setNames(paste0(gm_pc_spec$name_str, seq_along(pc_vars), 'pcs'))

# load boots
boots_pcs_ls  = lapply(seq_along(pc_vars), function(n_pcs) {
  # make file
  layer_spec  = make_layer_pc_spec(gm_pc_spec, pc_vars, n_pcs = n_pcs)
  boots_dt    = sprintf(boots_pat, layer_spec$name, date_tag) %>% fread
  
  return(boots_dt)
  }) %>% setNames(paste0(gm_pc_spec$name_str, seq_along(pc_vars), 'pcs'))
```

```{r combine_results}
ancom_ls      = c(ancom_ls, ancom_pcs_ls, list(lesions_NAGM_4pcs = ancom_nagm))
boots_ls      = c(boots_ls, boots_pcs_ls, list(lesions_NAGM_4pcs = boots_nagm))
```

```{r calc_pcs_coefs}
pcs_coefs_dt  = calc_pcs_coefs_dt(pc_vars, boots_ls, labels_dt) 
```

```{r calc_wm_oligo_grps}
# load hand-picked oligo groupings
olg_grps_dt   = fread(olg_grps_f)

# restrict to WM oligos, also cell types with min. no. of cells
input_dt      = conos_all %>%
  .[(matter == "WM") & (type_broad %in% olg_types)] %>%
  .[, n_type := .N, by = type_fine ] %>%
  .[ n_type >= min_cells ] %>% .[ order(sample_id, type_fine) ]

# calc clusters
set.seed(20211112)
olg_clust_dt  = calc_olg_grps_from_clrs(input_dt, k = 3, prior_size = 1e3)
fwrite(olg_clust_dt, file = olg_clust_f)
```

# Analysis

## Ctrl GM vs WM{.tabset}

```{r plot_wm_vs_gm, fig.height = 3, fig.width = 8, results = 'asis'}
for (m in c('nb', 'edger', 'poisson', 'beta')) {
  cat('### ', m, '\n')
  print(plot_wm_vs_gm(conos_dt, model = m))
  cat('\n\n')  
}
```

```{r plot_wm_vs_gm_absolute, fig.height = 6, fig.width = 8, results = 'asis'}
m = 'nb_w_absolute'
cat('### ', m, '\n')
print(plot_wm_vs_gm(conos_dt, model = m))
cat('\n\n')  
```

## Contribution of lesion type and donor to variability in celltype counts{.tabset}

```{r plot_lrt_results, fig.height = 4, fig.width = 8, results = 'asis', cache = FALSE}
for (nn in names(lrt_ls)) {
  cat('### ', nn, '\n')
  print(plot_lrt_results(nn, lrt_ls, labels_dt))
  cat('\n\n')  
}
```

## Abundance of _GPR17_-expressing cells in snRNAseq and IHC

```{r plot_no_gpr17_cells, fig.height = 7, fig.width = 4}
g_seq = plot_raw_cell_counts_sel_gene(prop_fine_f, conos_dt[ matter == "WM" ], sel_g) + 
  ggtitle('snRNAseq nuclei counts')
g_ihc = plot_gpr17_ihc(gpr17_dt[ matter == "WM" ]) + 
  ggtitle('GPR17 IHC staining')
g     = g_seq / g_ihc
print(g)
```

```{r test_gpr17_ihc}
# define models
frm_full  = "log(mean_per_mm2 + 0.1) ~ lesion_type + (1 | donor_id )"
frm_fix   = "log(mean_per_mm2 + 0.1) ~ lesion_type"
frm_null  = "log(mean_per_mm2 + 0.1) ~ 1"

message('evidence of lesion and donor effects in GPR17 IHC data\n')
for (this_m in c("WM", "GM")) {
  # restrict data
  data_dt   = gpr17_dt[ matter == this_m ] %>% 
    .[, lesion_type := fct_drop(lesion_type) ]

  # fit models
  fit_full  = glmmTMB::glmmTMB( as.formula(frm_full), 
    data = data_dt, family = 'gaussian')
  fit_fix   = update(fit_full, frm_fix)
  fit_null  = update(fit_full, frm_null)

  # compare
  message('  in ', this_m, ":")
  print(anova(fit_full, fit_fix, fit_null))
}
```

## Compositional grouping heatmaps

### Compositional groupings of samples (CLR){.tabset}

```{r plot_heatmaps_clr, fig.height = 10, fig.width = 8, results = 'asis'}
# plot CLR heatmaps
cat("#### WM\n")
  props_wm    = calc_props_dt(conos_dt[ type_broad %in% wm_types ], sample_vars) %>%
    .[ matter == "WM" ]
  hm_wm       = plot_clr_heatmap(props_wm, cluster_rows = TRUE, n_clusters = 5, what = 'clr')
  hm_wm       = draw(hm_wm, row_dend_width = unit(0.5, "in"), merge_legend = TRUE)
cat('\n\n')
cat("#### GM\n")
  props_gm    = calc_props_dt(conos_dt[ type_broad %in% gm_types ], sample_vars) %>%
    .[ matter == "GM" ]
  hm_gm       = plot_clr_heatmap(props_gm, cluster_rows = TRUE, n_clusters = 5, what = 'clr')
  hm_gm       = draw(hm_gm, row_dend_width = unit(0.5, "in"), merge_legend = TRUE)
cat('\n\n')

# save clusters
save_heatmap_clusters(hm_wm@ht_list[[1]], sprintf(clr_pat, "WM", date_tag))
save_heatmap_clusters(hm_gm@ht_list[[1]], sprintf(clr_pat, "GM", date_tag))
```

### Compositional groupings of samples (log proportions){.tabset}

```{r plot_heatmaps_log_p, fig.height = 10, fig.width = 8, results = 'asis'}
cat("#### WM\n")
  props_wm    = calc_props_dt(conos_dt[ type_broad %in% wm_types ], sample_vars) %>%
    .[ matter == "WM" ] # & neuro_ok == TRUE]
  draw(plot_clr_heatmap(props_wm, cluster_rows = FALSE, what = 'log_p'),
    row_dend_width = unit(0.5, "in"), merge_legend = TRUE)
cat('\n\n')
cat("#### GM\n")
  props_gm    = calc_props_dt(conos_dt[ type_broad %in% gm_types ], sample_vars) %>%
    .[ matter == "GM" ] # & neuro_ok == TRUE]
  draw(plot_clr_heatmap(props_gm, cluster_rows = FALSE, what = 'log_p'),
    row_dend_width = unit(0.5, "in"), merge_legend = TRUE)
cat('\n\n')
```

### Compositional groupings of samples (log proportions, oligodendroglia only){.tabset}

```{r plot_heatmaps_log_p_oligos, fig.height = 10, fig.width = 6, results = 'asis'}
cat("#### WM\n")
  props_wm    = calc_props_dt(conos_dt[ type_broad %in% olg_types ], sample_vars) %>%
    .[ matter == "WM" ] # & neuro_ok == TRUE]
  draw(plot_clr_heatmap(props_wm, cluster_rows = FALSE, what = 'log_p'),
    row_dend_width = unit(1, "in"), merge_legend = TRUE)
cat('\n\n')
cat("#### GM\n")
  props_gm    = calc_props_dt(conos_dt[ type_broad %in% olg_types ], sample_vars) %>%
    .[ matter == "GM" ] # & neuro_ok == TRUE]
  draw(plot_clr_heatmap(props_gm, cluster_rows = FALSE, what = 'log_p'),
    row_dend_width = unit(1, "in"), merge_legend = TRUE)
cat('\n\n')
```

### Compositional groupings of samples (log proportions, oligodendroglia only, ordered by patient){.tabset}

```{r plot_heatmaps_log_p_oligos_by_patient, fig.height = 10, fig.width = 5, results = 'asis'}
cat("#### WM\n")
  props_wm    = calc_props_dt(conos_dt[ type_broad %in% olg_types ], sample_vars) %>%
    .[ matter == "WM" ] # & neuro_ok == TRUE]
  draw(plot_clr_heatmap(props_wm, cluster_rows = FALSE, what = 'log_p', 
    order_subj = TRUE), row_dend_width = unit(1, "in"), merge_legend = TRUE)
cat('\n\n')
cat("#### GM\n")
  props_gm    = calc_props_dt(conos_dt[ type_broad %in% olg_types ], sample_vars) %>%
    .[ matter == "GM" ] # & neuro_ok == TRUE]
  draw(plot_clr_heatmap(props_gm, cluster_rows = FALSE, what = 'log_p', 
    order_subj = TRUE), row_dend_width = unit(1, "in"), merge_legend = TRUE)
cat('\n\n')
```

### Compositional groupings of samples (GM, CLR, neurons only)

```{r plot_heatmaps_log_p_neurons, fig.height = 11, fig.width = 9}
props_gm    = calc_props_dt(conos_dt[ type_broad %in% neu_types ], sample_vars) %>%
  .[ matter == "GM" ] %>%
  merge(wide_neu[, .(sample_id, ctrl_PC01, ctrl_PC02, ctrl_PC03, ctrl_PC04)], 
    by = 'sample_id')
draw(plot_clr_heatmap(props_gm, cluster_rows = FALSE, what = 'clr'),
  row_dend_width = unit(1, "in"), merge_legend = TRUE)
```

## Barplots of WM oligodendroglia proportions{.tabset}

```{r plot_sample_splits_bars_oligos, fig.height = 10, fig.width = 8, results = 'asis', cache = FALSE}
m = 'WM'
cat('### By hand\n')
print(plot_sample_propn_barplots(conos_dt[matter == m], olg_grps_dt,
  types = olg_types, show_broad = FALSE))
cat('\n\n')
cat('### CLR clusters\n')
print(plot_sample_propn_barplots(conos_dt[matter == m], olg_clust_dt,
  types = olg_types, show_broad = FALSE))
cat('\n\n')
cat('### By hand, no OPC/Oligo split\n')
print(plot_sample_propn_barplots(conos_dt[matter == m], olg_grps_dt,
  types = olg_types, show_broad = FALSE, split_broad = FALSE))
cat('\n\n')
```

## CLR plots of oligodendroglia{.tabset}

```{r plot_sample_splits_clrs_oligos, fig.height = 6, fig.width = 10, results = 'asis'}
# set up what we want
names_ls    = c("WM", "GM_all", "GM_all_resid", "GM_out", "GM_out_resid")
title_ls    = c("WM", "GM", "GM (w/o layers)", "GM (SD016/13 excluded)", 
  "GM (w/o layers, no SD016/13)") %>% setNames(names_ls)
matter_ls   = c("WM", "GM", "GM", "GM", "GM") %>% setNames(names_ls)
outs_ls     = c(FALSE, FALSE, FALSE, TRUE, TRUE) %>% setNames(names_ls)
layers_ls   = c(FALSE, FALSE, TRUE, FALSE, TRUE) %>% setNames(names_ls)
outlier_ls  = "SD016/13"
sel_pcs     = c("ctrl_PC01", "ctrl_PC02", "ctrl_PC03", "ctrl_PC04")

# run through
for (nn in names_ls) {
  # do title
  cat('### ', title_ls[[ nn ]], '\n')

  # get data
  input_dt  = conos_dt %>%
    .[(matter == matter_ls[[ nn ]]) & (type_broad %in% olg_types)]

  # remove outliers?
  if (outs_ls[[nn]])
    input_dt  = input_dt[ !(subject_id %in% outlier_ls) ]

  # restrict to samples with min. no. of cells
  input_dt  = input_dt[, n_type := .N, by = type_fine ] %>%
    .[ n_type >= min_cells ] %>% .[ order(sample_id, type_fine) ]

  # do either with or without layers
  if (layers_ls[[ nn ]]) {
    suppressWarnings(print(plot_sample_clrs_layers(input_dt, 
      all_pcs_dt[, c("sample_id", sel_pcs), with = FALSE ])))
  } else {
    suppressWarnings(print(plot_sample_clrs(input_dt, prior_size = 1e3)))
  }

  cat('\n\n')
}

# plot
cat('### ', 'WM w oligo grps by hand', '\n')
  # annotate with oligo groups 
  input_dt  = conos_dt %>%
    .[(matter == "WM") & (type_broad %in% olg_types)] %>%
    merge(olg_grps_dt, by = "subject_id") %>%
    .[, n_type := .N, by = type_fine ] %>%
    .[ n_type >= min_cells ] %>% .[ order(sample_id, type_fine) ]

  # plot
  suppressWarnings(print(plot_sample_clrs(input_dt, prior_size = 1e3)))
cat('\n\n')

# add CLR clustering
cat('### ', 'WM w oligo grps from CLR clusters', '\n')
  # annotate with oligo groups 
  input_dt  = conos_dt %>%
    .[(matter == "WM") & (type_broad %in% olg_types)] %>%
    merge(olg_clust_dt, by = "sample_id") %>%
    .[, n_type := .N, by = type_fine ] %>%
    .[ n_type >= min_cells ] %>% .[ order(sample_id, type_fine) ]

  # plot
  suppressWarnings(print(plot_sample_clrs(input_dt, prior_size = 1e3)))
cat('\n\n')
```

## GM layers

### Proportions with layers

```{r plot_propns_layers, fig.height = 9, fig.width = 5}
(plot_propns_layers(props_dt[ matter == "GM" & str_detect(type_broad, 'neuron') ]))
```

### PCA of proportions, neurons only, all samples

```{r plot_clr_pca_neurons, fig.height = 8, fig.width = 8}
(plot_pca_results(wide_neu, ctrl_pcs_dt, pc_vars, what = "proj"))
```

### Patients over PC1

```{r plot_patients_over_pcs, fig.height = 8, fig.width = 10}
(plot_patients_over_pc(wide_neu, pc_vars))
```

### PC variance explained and layer correlations

```{r plot_layer_var_exp, fig.height = 4, fig.width = 5}
(plot_pca_loadings(ctrl_pcs_dt, cut_var_exp = cut_var_exp, 
  cut_layer_cor = cut_layer_cor))
```

## ANCOM-BC results

### ANCOM-BC standard results{.tabset}

```{r plot_standard_all_coefs, fig.height = 11, fig.width = 7, results = 'asis'}
for (nn in names(ancom_ls)) {
  cat('#### ', nn, '\n')
  print(plot_ancombc_ci(ancom_ls[[nn]], q_cut = 0.05))
  cat('\n\n')  
}
```

### ANCOM-BC standard results, lesions only{.tabset}

```{r plot_standard_lesions, fig.height = 11, fig.width = 7, results = 'asis'}
for (nn in names(ancom_ls)) {
  cat('#### ', nn, '\n')
  print(plot_ancombc_ci(ancom_ls[[nn]], coef_filter = "lesion_type", q_cut = 0.05))
  cat('\n\n')  
}
```

### ANCOM-BC bootstrap results{.tabset}

```{r plot_bootstraps_all_coefs, fig.height = 11, fig.width = 7, results = 'asis'}
for (nn in names(boots_ls)) {
  cat('#### ', nn, '\n')
  print(plot_boots_dt(boots_ls[[nn]]))
  cat('\n\n')  
}
```

### ANCOM-BC bootstrap results, lesions only{.tabset}

```{r plot_bootstraps_lesions, fig.height = 12, fig.width = 8, results = 'asis', cache = FALSE}
for (nn in names(boots_ls)) {
  cat('#### ', nn, '\n')
  print(plot_boots_dt(boots_ls[[nn]], coef_filter = "lesion_type", 
    ancom_obj = ancom_ls[[nn]], meta_dt = meta_dt))
  cat('\n\n')  
}
```

### ANCOM-BC bootstrap results, lesions, significant only{.tabset}

```{r plot_bootstraps_lesions_signif, fig.height = 7, fig.width = 5, results = 'asis', cache = FALSE}
for (nn in names(boots_ls)) {
  cat('#### ', nn, '\n')
  print(plot_boots_dt(boots_ls[[nn]], 
    coef_filter = "lesion_type", signif_only = TRUE))
  cat('\n\n')  
}
```

### Bootstrap vs standard{.tabset}

```{r plot_bootstrap_vs_standard, fig.height = 6, fig.width = 12, results = 'asis'}
for (nn in names(boots_ls)) {
  cat('#### ', nn, '\n')
  print(plot_boots_vs_standard(boots_ls[[nn]], ancom_ls[[nn]], labels_dt,
    q_cut = 0.05))
  cat('\n\n')  
}
```

### Effect of including PCs, neurons only{.tabset}

```{r plot_effect_of_pcs_neurons, fig.height = 5, fig.width = 12, results = 'asis'}
for (sel_coef in unique(pcs_coefs_dt$coef)) {
  cat('#### ', sel_coef, '\n')
  print(plot_effect_of_pcs(sel_coef, 
    pcs_coefs_dt[ type_broad %in% gm_pc_spec$broad_sel ] ))
  cat('\n\n')
}
```

### Effect of including PCs, other celltypes{.tabset}

```{r plot_effect_of_pcs_rest, fig.height = 5, fig.width = 12, results = 'asis'}
for (sel_coef in unique(pcs_coefs_dt$coef)) {
  cat('#### ', sel_coef, '\n')
  print(plot_effect_of_pcs(sel_coef, 
    pcs_coefs_dt[ !(type_broad %in% gm_pc_spec$broad_sel) ]))
  cat('\n\n')
}
```

### Effect of including PCs, lesions only{.tabset}

```{r plot_effect_of_pcs_lesions, fig.height = 8, fig.width = 12, results = 'asis'}
sel_coefs   = c("NAGM", "GML")
cat('#### ', 'neurons only', '\n')
  g = plot_effect_of_pcs(sel_coefs, 
    pcs_coefs_dt[ type_broad %in% gm_pc_spec$broad_sel ] ) +
    labs(title = "Neurons only")
  print(g)
cat('\n\n')
cat('#### ', 'other celltypes', '\n')
  g = plot_effect_of_pcs(sel_coefs, 
    pcs_coefs_dt[ !(type_broad %in% gm_pc_spec$broad_sel) ] ) +
    labs(title = "Non-neurons only")
  print(g)
cat('\n\n')
```

# Outputs

```{r save_pseudobulk_w_pcs}
if ( !all(file.exists(pb_pcs_ls)) ) {
  for (nn in names(pb_f_ls)) {
    # which files?
    pb_f        = pb_f_ls[[ nn ]]
    pb_pcs_f    = pb_pcs_ls[[ nn ]]

    # load full pseudobulk, restrict to just GM
    pb_all      = pb_f %>% readRDS
    pb_pcs      = pb_all[, all_pcs_dt$sample_id]
    for (v in str_subset(names(all_pcs_dt), "ctrl_PC"))
      colData(pb_pcs)[[v]] = all_pcs_dt[[v]] %>% scale %>% `/`(2)

    # save
    saveRDS(pb_pcs, file = pb_pcs_f)    
  }
}
```

```{r session_info, include=TRUE, echo=TRUE, results='markup'}
devtools::session_info()
```
