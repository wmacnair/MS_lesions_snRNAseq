# ms03_SampleQC.R

suppressPackageStartupMessages({
  library('magrittr')
  library('data.table')
  library('forcats')
  library('assertthat')
  library('stringr')

  library('ggplot2')
  library('scales')
  library('patchwork')
  library('ComplexHeatmap')
  library('ggbeeswarm')

  library('loomR')
  library('Matrix')
  library('SingleCellExperiment')
  library('SampleQC')
})

make_qc_dt_file <- function(sce_f, qc_f, overwrite=FALSE) {
  # check if already done
  if (file.exists(qc_f) & overwrite == FALSE)
    return(fread(qc_f))

  # load sce file
  sce         = sce_f %>% readRDS

  # calculate reads
  all_counts  = sce %>% counts %>% Matrix::colSums(.)

  # calculate mito reads
  mt_idx      = rowData(sce)$symbol %>% 
    str_detect('^MT-')
  assert_that( sum(mt_idx) == 13 )
  mt_counts   = sce[mt_idx, ] %>% counts %>% Matrix::colSums(.)

  # calculate no. features
  all_feats   = sce %>% counts %>% `>`(0) %>% Matrix::colSums(.)

  # assemble
  qc_dt = data.table(
    cell_id     = colnames(sce),
    all_counts  = all_counts,
    all_feats   = all_feats,
    mito_counts = mt_counts
    )
  fwrite(qc_dt, file=qc_f)

  return(qc_dt)
}

get_cols_dt <- function(sce_f, cols_f, overwrite=FALSE) {
  # check if already done
  if (file.exists(cols_f) & overwrite == FALSE)
    return(fread(cols_f))

  # load sce file
  sce     = sce_f %>% readRDS

  # assemble
  cols_dt   = colData(sce) %>% as.data.frame %>%
    as.data.table(keep.rownames = 'cell_id')
  fwrite(cols_dt, file=cols_f)

  return(cols_dt)
}

extract_splice_ratios <- function(loom_dir, save_dir) {
  # get all loom files
  loom_fs   = dir(loom_dir, '.loom$')
  loom_dirs   = list.dirs(loom_dir, full.names=FALSE) %>%
    .[ . != "" ]

  # define sample_id regex
  save_pat  = 'splice_ratio_%s.txt'
  cell_id_re  = '^[^:]+:([ATCG]+)x$'

  # loop through each
  message('extracting spliced, unspliced data')
  for (s in loom_dirs) {
    message(s, '..', appendLF=FALSE)

    # check if already saved
    save_f  = file.path(save_dir, sprintf(save_pat, s))
    if (file.exists(save_f))
      next

    # if not, connect to loom file
    tryCatch({
      l       = connect(filename = file.path(loom_dir, s, paste0(s, '.loom')), 
        mode = "r+", skip.validate = TRUE)
    }, error=function(err) {
      message('  opening loom failed for ', s, '  ', appendLF=FALSE)
    })
    if (!exists('l'))
      next
    spliced   = l[['layers/spliced']]
    unspliced   = l[['layers/unspliced']]
    cell_ids  = l[['col_attrs/CellID']][]

    # some checks
    assert_that(
      spliced$dims[1] == unspliced$dims[1],
      spliced$dims[2] == unspliced$dims[2],
      spliced$dims[1] == length(cell_ids)
      )
    
    # calculate totals
    dt = data.table(
      sample_id   = s,
      barcode   = paste0(str_match(cell_ids, cell_id_re)[, 2], '-1'),
      spliced   = Matrix::rowSums(spliced[,]),
      unspliced   = Matrix::rowSums(unspliced[,])
      )

    # save results
    fwrite(dt, file=save_f)

    # tidy up
    rm(l)
  }
  message()
}

get_spliced_dt <- function(qc_dir) {
  spliced_f = file.path(qc_dir, 'spliced_dt.txt')
  if (file.exists(spliced_f))
    return(fread(spliced_f))

  # get all loom files
  all_fs    = dir(qc_dir, '.txt')
  txt_re    = 'splice_ratio_(WM|EU)[0-9]{3}.txt'
  splice_fs   = all_fs %>% str_subset(txt_re)

  spliced_dt  = file.path(qc_dir, splice_fs) %>% 
    lapply(fread) %>%
    rbindlist %>%
    .[, splice_ratio  := log2(spliced + 1) - log2(unspliced + 1) ] %>%
    .[, matter      := str_detect(sample_id, 'WM') %>% 
      ifelse('WM', 'GM') %>% 
      factor(levels=c('WM', 'GM'))
      ] %>%
    .[, cell_id := paste0(sample_id, ':', barcode) ] %>%
    setcolorder('cell_id')

  spliced_dt %>% fwrite(file = spliced_f)

  return(spliced_dt)
}

get_qc_dt <- function(qc_dir, qc_f) {
  # get spliced results
  spliced_dt    = get_spliced_dt(qc_dir)

  # get standard QC outputs
  qc_dt = fread(qc_f) %>%
    .[, .(cell_id, sample_id=str_match(cell_id, '((EU|WM)[0-9]{3})')[,2], 
      log_counts  = log10(all_counts), 
      log_feats   = log10(all_feats),
      logit_mito  = qlogis((mito_counts+1) / (all_counts + 2))
      )] %>%
  merge(spliced_dt[, .(cell_id, splice_ratio)], by='cell_id')
  
  return(qc_dt)
}

plot_spliced_distns <- function(spliced_dt) {
  splice_bks   = seq(-4, 6, 2)
  splice_labs  = ifelse(splice_bks < 0, 
    sprintf('1:%d', 2^abs(splice_bks)),
    sprintf('%d:1', 2^abs(splice_bks))
    )
  g = ggplot(spliced_dt) +
    aes( x=splice_ratio, group=sample_id ) +
    geom_line( alpha=0.5, stat='density', adjust=0.5 ) +
    scale_x_continuous( breaks = splice_bks, labels = splice_labs ) +
    scale_y_continuous( breaks=pretty_breaks() ) +
    facet_grid( matter ~ ., scales = 'free_y' ) +
    coord_cartesian( xlim=c(-4,6) ) +
    theme_bw() +
    labs( x='spliced:unspliced ratio (+ve = more spliced)' )

  return(g)
}

plot_qc_metric_scatter <- function(dt, qc_names) {
  melt_dt = dt %>% 
    melt(measure=qc_names, value.name='qc_val', variable.name='qc_var') %>%
    .[, qc_var := factor(qc_var, levels=qc_names)]
  plot_dt = merge(
    melt_dt[, .(cell_id, qc_x=qc_var, val_x=qc_val)],
    melt_dt[, .(cell_id, qc_y=qc_var, val_y=qc_val)],
    by='cell_id', allow.cartesian=TRUE
    ) %>% .[ as.integer(qc_x) > as.integer(qc_y) ]
  # get sample name
  sel_s   = unique(dt$library_id)
  g = ggplot(plot_dt) + aes( x=val_x, y=val_y ) +
    geom_bin2d() + scale_fill_distiller( palette='RdBu', trans='log10' ) +
    facet_grid( qc_y ~ qc_x, scales='free' ) + theme_bw() +
    labs(
      x     = 'QC metric 1',
      y     = 'QC metric 2',
      fill  = 'no. cells',
      title   = sel_s
      )

  return(g)
}

plot_totals_split_by_meta <- function(pre_dt, post_dt, meta_dt) {
  # define age splits
  age_breaks  = c(0, 40, 50, 60, 70, 80, 100)
  age_labels  = paste0('<=', age_breaks[-1])
  yrs_breaks  = c(0, 10, 20, 30, 40, 60)
  yrs_labels  = paste0(yrs_breaks[-length(yrs_breaks)], ' to ', yrs_breaks[-1])

  # load metadata
  meta_dt     = copy(meta_dt) %>%
    .[, lesion_type := lesion_type %>% 
      fct_recode(`WM (ctrl)` = "WM", `GM (ctrl)` = "GM")] %>%
    .[, age_cat     := cut(age_at_death, breaks = age_breaks, 
      labels = age_labels) %>% factor(levels = age_labels) ] %>%
    .[, yrs_w_ms    := cut(years_w_ms, breaks = yrs_breaks, 
      labels = yrs_labels) %>% factor(levels = yrs_labels) ] %>%
    .[, .(sample_id, subject_id, matter, lesion_type, diagnosis, 
      sex, age_cat, yrs_w_ms, pmi_cat, brain_bank = sample_source, seq_pool)]

  # join to keep totals
  pre_n_dt    = pre_dt[, .(pre_n = .N), by = sample_id]
  post_n_dt   = post_dt[, .(post_n = .N), by = sample_id]
  keep_n_dt   = merge(pre_n_dt, post_n_dt, by = 'sample_id', all.x = TRUE) %>%
    .[ is.na(post_n), post_n := 0 ] %>%
    merge(meta_dt, by = 'sample_id')

  # pick which to show, get their values
  meta_vars   = c('matter', 'lesion_type', 'diagnosis', 'sex', 'age_cat',
    'yrs_w_ms', 'pmi_cat', 'brain_bank', 'seq_pool')
  level_ord   = meta_vars %>%
    lapply(function(v) levels(factor(keep_n_dt[[v]]))) %>%
    do.call(c, .)

  # melt by variable
  tmp_dt      = keep_n_dt %>%
    melt(id = c('sample_id', 'subject_id', 'pre_n', 'post_n'), 
      measure = meta_vars, variable.name = 'meta_var', value.name = 'meta_val') %>%
    .[, meta_val := factor(meta_val, levels=level_ord)] %>%
    melt(measure = c('pre_n', 'post_n'), 
      value.name = 'n_cells', variable.name = 'qc_status') %>%
    .[n_cells > 0, .(
      n_cells   = round( sum(n_cells) / 1e3 ) %>% as.integer, 
      n_samples = .N, 
      n_donors  = length(unique(subject_id))
      ), by = .(meta_var, meta_val, qc_status)] %>%
    melt(measure = c('n_cells', 'n_samples', 'n_donors'),
      value.name = 'count_val', variable.name = 'count_var')

  # calculate lost cells etc
  lost_dt     = merge(
    tmp_dt[qc_status == 'pre_n', .(meta_var, meta_val, count_var, pre_val=count_val)],
    tmp_dt[qc_status == 'post_n', .(meta_var, meta_val, count_var, post_val=count_val)],
    by = c('meta_var', 'meta_val', 'count_var')
    ) %>%
    .[, lost_val := pre_val - post_val]
  all_dt      = rbind(
    tmp_dt[qc_status == 'post_n', .(meta_var, meta_val, qc_status = 'kept', count_var, count_val)],
    lost_dt[, .(meta_var, meta_val, qc_status = 'excluded', count_var, count_val=lost_val)]
    ) %>% .[, qc_status := qc_status %>% factor(levels=c('excluded', 'kept')) ] %>% 
    .[, count_var := count_var %>% fct_recode(`#k cells` = 'n_cells', 
      `# samples` = 'n_samples', `# donors` = 'n_donors') ]

  # plot
  g = ggplot(all_dt[count_val > 0]) + 
    aes( y = fct_rev(meta_val), x = count_val, 
      label = count_val, fill = qc_status ) +
    geom_col(colour = NA) + 
    geom_text(colour = 'black', position = position_stack(vjust = 0.5), size = 3) +
    scale_fill_manual(values = c(kept = 'grey50', excluded = 'grey80'), 
      breaks = c('kept', 'excluded')) +
    scale_x_continuous(breaks = pretty_breaks()) +
    facet_grid( meta_var ~ count_var, scales='free', space='free_y') +
    theme_bw() + theme( panel.grid = element_blank() ) +
    labs(y = NULL, x = NULL, fill = 'QC status', 
      title = 'Summary of cells, samples and donors retained')

  return(g)
}

# # how to plot just annotations (by making 0*0 heatmap matrix):

# hc = hclust(dist(matrix(rnorm(100), 10)))
# Heatmap(matrix(nc = 0, nr = 10), cluster_rows = hc, 
#     right_annotation = rowAnnotation(
#         foo = anno_points(1:10),
#         sth = 1:10,
#         bar = anno_barplot(1:10)),
#     row_split = 2)

calc_summary_dt <- function(input_dt, meta_f) {
  # calculate sums split by healthy / MS
  meta_dt     = fread(meta_f) %>% 
    setnames('library_id', 'sample_id') %>%
    .[, matter    := factor(matter, levels=matter_ord) ] %>%
    .[, condition := ifelse(lesion_type %in% c('WM', 'GM'), 
      'healthy', 'MS') %>% factor ] %>%
    .[, .(sample_id, patient_id, matter, condition)]

  # join to keep totals
  keep_n_dt   = input_dt[, .(n_cells = .N), by=sample_id] %>%
    merge(meta_dt, by='sample_id')

  # melt by variable
  summary_dt  = keep_n_dt %>%
    .[, .(
      n_cells     = sum(n_cells), 
      n_samples   = .N, 
      n_patients  = length(unique(patient_id))
      ), by = .(matter, condition)] %>%
    setorder('matter', 'condition')

  return(summary_dt)
}

plot_qc_summary_heatmap <- function(qc_stats, meta_input) {
  # make matrix of z-scores
  stats_tmp = copy(qc_stats) %>%
    .[ sample_id %in% meta_input$sample_id ] %>%
    .[, z := scale(med_val), by = qc_var ] %>%
    .[ qc_var %in% c('logit_mito', 'splice_ratio'), z := z * -1 ]

  # make matrix of z-scores
  z_wide    = stats_tmp %>%
    dcast.data.table( sample_id ~ qc_var, value.var = 'z')
  z_mat     = z_wide[, -'sample_id', with = FALSE] %>%
    as.matrix %>% set_rownames(z_wide$sample_id)

  # make matrix of text labels
  lab_wide  = stats_tmp %>%
    .[ qc_var == "log10_N",
      lab := sprintf("%0.1fk", 10^med_val / 1e3) ] %>%
    .[ qc_var == "log_counts",
      lab := sprintf("%0.1fk", 10^med_val / 1e3) ] %>%
    .[ qc_var == "log_feats",
      lab := sprintf("%0.1fk", 10^med_val / 1e3) ] %>%
    .[ qc_var == "logit_mito",
      lab := sprintf("%0.1f%%", plogis(med_val) * 1e2) ] %>%
    .[ qc_var == "splice_ratio",
      lab := sprintf("%0.0f%%", 1/(1 + 2^med_val) * 1e2) ] %>%
    dcast.data.table( sample_id ~ qc_var, value.var = 'lab')
  lab_mat   = lab_wide[, -'sample_id', with = FALSE] %>%
    as.matrix %>% set_rownames(lab_wide$sample_id)

  # lots of colours
  z_cols    = cols_fn(seq(-2, 2, 0.2), 0.2, 'RdBu', 
    pal_dir = -1, range = 'symmetric')

  # define function for labelling
  labelling_fn <- function(j, i, x, y, width, height, fill) {
    grid.text(sprintf("%s", lab_mat[i, j]), x, y, 
      gp = gpar(fontsize = 6))
  }

  # make sample annotations
  rows_dt   = copy(meta_input) %>% setkey('sample_id') %>%
    .[rownames(z_mat)]

  # do colours for patients
  pats_dt     = meta_input[, .(subject_n = .N), by = subject_id] %>% 
    .[ subject_n > 1, .(subject_id, subject_n) ] %>% .[ order(-subject_n)]
  pat_list    = pats_dt$subject_id
  pat_cols    = nice_cols[seq.int(length(pat_list))] %>% setNames(pat_list)
  donor_vals  = rows_dt$subject_id
  donor_vals[ !(donor_vals %in% names(pat_cols)) ] = NA
  les_vals    = rows_dt$lesion_type %>% fct_drop %>% levels
  les_cols    = brewer.pal(length(les_vals), 'BrBG') %>% 
    setNames(les_vals)

  # make col annotations
  row_annots  = rowAnnotation(
    lesion    = rows_dt$lesion_type, 
    diagnosis = rows_dt$diagnosis, 
    donor     = donor_vals, 
    sex       = rows_dt$sex, 
    age       = rows_dt$age_at_death, 
    pmi_cat   = rows_dt$pmi_cat, 
    col     = list(
      lesion    = les_cols,
      diagnosis = disease_cols,
      donor     = pat_cols,
      sex       = c(M = 'black', F = 'white'),
      age       = cols_fn(rows_dt$age_at_death, 10, 'Greys', 
        pal_dir = 1, range='natural'),
      pmi_cat   = c(up_to_6H = '#f7fcf5', `6H_to_12H` = '#41ab5d', 
        over_12H = '#005a32')
    ), 
    annotation_name_side='top', show_legend = c(donor = FALSE)
  )

  # # split rows by MS / not
  # row_split   = ifelse(rows_dt$diagnosis == 'CTR', 'CTR', 'MS')

  # do column titles
  var_lookup  = c(
    log10_N       = "no. cells", 
    log_counts    = "library size", 
    log_feats     = "no. features", 
    logit_mito    = "mito pct", 
    splice_ratio  = "pct unspliced"
  )

  # do heatmap for these genes
  name_str  = "median\nQC value\n(z-scored,\nsign flipped:\n+ve = good)"
  hm_obj    = Heatmap(
    matrix = z_mat, col = z_cols, cell_fun = labelling_fn,
    column_labels = var_lookup[ colnames(z_mat) ],
    cluster_rows = TRUE, cluster_columns = FALSE,
    row_names_gp = gpar(fontsize = 7),
    name = name_str, heatmap_legend_param = list(title = name_str),
    left_annotation = row_annots, 
    # row_split = row_split, cluster_row_slices = FALSE,
    row_names_side = "left", column_names_side = "top")

  return(hm_obj)
}

plot_ctrl_vs_ms_metadata <- function( meta_tmp, to_show ) {
  # take values per patient
  pats_dt   = meta_dt[, c('subject_id', 'matter', to_show), with = FALSE ] %>% 
    unique

  # do plot for each of WM, GM
  matter_lvls   = c("WM", "GM")
  g   = lapply(matter_lvls, function(this_m) {
    # get just these samples
    pats_sel  = pats_dt[ matter == this_m ] %>%
      .[, ms_status := ifelse(diagnosis != "CTR", "MS", "CTR") %>% factor ]
    g_sel     = lapply(to_show, function(v) {
      # decide what to do
      if (v %in% c('age_at_death', 'pmi_minutes')) {
        g = ggplot(pats_sel) +
          aes_string( x = "ms_status", y = v ) +
          geom_quasirandom(size = 2, shape = 21, fill = 'grey80') +
          theme_bw() + theme( panel.grid = element_blank() ) +
          coord_flip() +
          labs( x = NULL )
      } else if (v %in% c('diagnosis', 'sex', 'sample_source')) {
        plot_dt   = pats_sel[, .N, by = c('ms_status', v)]
        g = ggplot(plot_dt) +
          aes_string( y = "ms_status", x = v, label = "N" ) +
          geom_point( aes(size = N), shape = 21, fill = 'grey80' ) + 
          scale_size(range = c(0, 10), guide = "none") +
          geom_text() +
          theme_bw() + theme( panel.grid = element_blank() ) +
          labs( y = NULL )
      }
      # add title to first
      if (v == to_show[[1]])
        g = g + ggtitle(paste0(this_m, " donors"))
      return(g)
      }) %>% wrap_plots(ncol = 1)

    return(g_sel)
    }) %>% wrap_plots(nrow = 1) 

  return(g)
}

plot_lesions_per_donor <- function(keep_dt, meta_dt, to_show) {
  if (FALSE) {
    # define unit for each point
    pt_unit   = 0.2

    # restrict to just samples
    plot_dt   = meta_dt[ sample_id %in% unique(keep_dt$sample_id) ] %>%
      .[ lesion_type %in% to_show ] %>%
      .[, lesion_type := lesion_type %>% fct_drop ] %>%
      .[, .(sample_id, lesion_type, subject_id) ] %>% unique %>%
      # .[, .N, by = .(lesion_type, subject_id)] %>%
      .[, prop_gm := mean(lesion_type %in% c("NAGM", "GML")), by = subject_id ] %>%
      .[, n_total := .N, by = subject_id ] %>%
      .[ order(prop_gm, -n_total) ] %>%
      .[, subject_id  := fct_inorder(subject_id) ] %>%
      .[, n_tmp       := .N, by = .(subject_id, lesion_type)] %>%
      .[, y_delta     := seq(0, .N - 1) - (.N - 1) / 2, by = .(subject_id, lesion_type)] %>%
      .[ n_tmp == 1, y_delta := 0 ] %>%
      .[, y_val       := as.integer(lesion_type) + y_delta * pt_unit ]

    # get lesion values
    lesion_lvls   = levels(plot_dt$lesion_type)

    # do plot
    g = ggplot(plot_dt[ n_total > 1 ]) + 
      aes( x = subject_id, y = y_val ) +
      geom_point( colour = 'black' ) +
      scale_y_continuous( breaks = seq_along(lesion_lvls), labels = lesion_lvls, 
        trans = 'reverse' ) +
      theme_bw() + 
      theme( panel.grid = element_blank(), axis.text.x = element_blank() ) +
      labs( x = 'donor', y = 'lesion type', size = 'no. samples' )

  } else {
    # restrict to just samples
    plot_dt   = meta_dt[ sample_id %in% unique(keep_dt$sample_id) ] %>%
      .[ lesion_type %in% to_show ] %>%
      .[, lesion_type := lesion_type %>% fct_drop ] %>%
      .[, .(sample_id, lesion_type, subject_id, matter) ] %>% unique %>%
      .[, .N, by = .(lesion_type, subject_id, matter) ] %>% 
      .[, prop_gm := mean(matter == "GM"), by = subject_id ] %>%
      .[, n_total := sum(N), by = subject_id ] %>%
      .[ order(prop_gm, -n_total, lesion_type) ] %>%
      .[, subject_id  := fct_inorder(subject_id) ]

    # do plot
    g = ggplot(plot_dt[ n_total >= 4 ]) + 
    # g = ggplot(plot_dt) + 
      aes( x = subject_id, y = fct_rev(lesion_type) ) +
      # geom_tile( colour = 'black' ) +
      geom_tile( fill = 'black', colour = 'white' ) +
      theme_bw() + 
      # scale_fill_brewer( palette = 'Blues' ) +
      facet_grid( matter ~ ., scales = 'free_y', space = 'free_y' ) +
      theme( panel.grid = element_blank(), axis.text.x = element_blank(),
        strip.background = element_blank() ) +
      # labs( x = 'donor', y = 'lesion type', fill = 'no. samples' )
      labs( x = 'donor', y = 'lesion type' )

  }

  return(g)
}

make_sampleqc_report_pdf <- function(qc_obj, save_dir, proj_name) {
  # checks
  assert_that( dir.exists(save_dir) )
  # start_dir   = getwd()
  # setwd(save_dir)

  # define output file
  report_file = paste0('SampleQC_report_', proj_name, '.pdf')

  # render
  tmplt_file  = "analysis/SampleQC_report_template_pdf.Rmd"
  message('rendering ')
  tryCatch({
    rmarkdown::render(tmplt_file, output_format = 'pdf_document', 
      output_file = report_file, 
      output_dir = save_dir, 
      quiet = FALSE)
  }, error=function(cond) {
    message("Something went wrong with rendering your report :/")
    message("Here's the original error message:")
    message(cond)
    message()
    return(NA)
  })
}
