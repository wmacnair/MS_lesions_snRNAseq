# ms10_muscat_slurm_jobs.py
# code to submit nicely formatted slurm job
# python code/ms10_slurm_jobs.py run09

import os
import argparse
from datetime import datetime
import string
import subprocess


def submit_slurm(run_tag, n_days, n_cores):
  # make slurm file
  template_f  = "code/ms10_muscat_slurm_template.slurm"
  slurm_f     = write_slurm_file(template_f, run_tag, n_days, n_cores)

  # execute it
  bash_cmd    = f"sbatch {slurm_f}"
  subprocess.run(bash_cmd.split())


def write_slurm_file(template_f, run_tag, n_days, n_cores):
  # define things to substitute in the slurm template
  date_str    = datetime.today().strftime('%Y-%m-%d')
  short_name  = run_tag
  full_name   = f"muscat_{run_tag}_{date_str}"
  if n_days == 1:
    duration  = "23:59:00"
  elif n_days == 2:
    duration  = "47:59:00"
  elif n_days == 3:
    duration  = "71:59:00"
  elif n_days == 4:
    duration  = "95:59:00"
  else:
    raise ValueError

  # load template
  with open(template_f) as t:
    template = string.Template(t.read())

  # substitute!
  slurm_code  = template.substitute(
    short_name  = short_name,
    full_name   = full_name,
    n_cores     = n_cores,
    duration    = duration,
    run_tag     = run_tag
    )

  # write to new file
  slurm_f     = f"./code/jobs/{full_name}.slurm"
  with open(slurm_f, "w") as output:
    output.write(slurm_code)

  return slurm_f


if __name__ == '__main__':
  # define arguments
  parser = argparse.ArgumentParser()
  parser.add_argument('run_tag', type = str, 
    choices = ['run09', 'run10', 'run11', 'run12', 'run19', 'run20', 'run23', 
      'run24', 'run25'])
  parser.add_argument("--n_days", type = int, default = 1, choices = [1, 2, 3, 4])
  parser.add_argument("--n_cores", type = int, default = 24)

  # get arguments
  args = parser.parse_args()

  # call slurm thing
  submit_slurm(args.run_tag, args.n_days, args.n_cores)
