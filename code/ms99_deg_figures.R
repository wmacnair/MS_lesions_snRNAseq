# ms99_deg_figures.R
suppressPackageStartupMessages({
  library('fastcluster')
})

source('code/ms10_muscat_runs.R')

load_genes_from_genesets <- function(sel_set, sel_terms) {
  # get pathways
  pathways  = paths_list[[ sel_set ]] %>% gmtPathways

  # get genes
  assert_that( all(sel_terms %in% names(pathways)) )
  sel_gs_dt = lapply(sel_terms, function(s) 
    data.table(pathway = s, symbol = pathways[[ s ]]) ) %>% rbindlist

  return(sel_gs_dt)
}

calc_gene_causes_dt <- function(filter_dt) {
  # turn into 
  causes_dt   = filter_dt %>%
    .[ cluster_id %in% sel_cl ] %>%
    .[, is_ms := ms_effect == "big" & ms_signif == "signif" ] %>%
    .[, is_pt := pt_signif == "signif" & pt_variab == "variable" ] %>%
    .[ (is_ms == FALSE) & (is_pt == FALSE), cause := 'none found' ] %>%
    .[ (is_ms == TRUE) & (is_pt == FALSE),  cause := 'MS effect only' ] %>%
    .[ (is_ms == FALSE) & (is_pt == TRUE),  cause := 'donor effect only' ] %>%
    .[ (is_ms == TRUE) & (is_pt == TRUE),   cause := 'both' ] %>%
    .[, cause := factor(cause, 
      levels = c('none found', 'MS effect only', 'both', 'donor effect only')) ]

  return(causes_dt)
}

calc_fc_clusters <- function(input_dt, max_fdr = 0.05, logfc_cut = log(4), min_in_cl = 5) {
  # check inputs
  assert_that( max_fdr > 0 & max_fdr < 1 )

  # filter some genes
  sprintf('removing genes with best FDR > %d%%', 
    round(100 * max_fdr)) %>% message
  input_dt = input_dt %>%
    .[, best_fdr := min(padj), by = .(cluster_id, gene_id)] %>%
    .[ best_fdr < max_fdr ]

  # get list of clusters to fit
  cl_ls     = input_dt$cluster_id %>% unique %>% as.character

  # fit each cluster
  fc_cl_dt  = lapply(cl_ls, function(cl) {
    # turn into matrix
    tmp_dt    = input_dt[ cluster_id == cl ] %>%
      dcast.data.table( gene_id ~ test_var, value.var = 'logFC' )
    tmp_mat   = tmp_dt[ , -'gene_id', with = FALSE ] %>%
      as.matrix %>% set_rownames( tmp_dt$gene_id )

    # cluster matrix
    tmp_d     = dist(tmp_mat, method = "euclidean")
    hc_obj    = hclust(tmp_d, method = "complete")
    cl_vec    = cutree(hc_obj, h = logfc_cut * sqrt(ncol(tmp_mat)))

    # melt data and annotate with clusters
    clust_lu  = data.table(gene_id = rownames(tmp_mat), fc_clust_all = cl_vec) %>%
      .[, n_clust := .N, by = fc_clust_all]
    dt_melt   = tmp_dt %>% 
      melt.data.table(id = 'gene_id', 
        variable.name = 'lesion_type', value.name = 'logFC') %>%
      merge(clust_lu, by = 'gene_id')

    # put cluster in nice order, add to what we had
    clust_ord = dt_melt[ n_clust >= min_in_cl ] %>%
      .[, .(mean_logfc = mean(logFC)), by = fc_clust_all ] %>%
      .[ order(-mean_logfc) ] %>%
      .[, new_clust := 1:.N ]
    dt_melt   = merge(dt_melt, clust_ord, by = 'fc_clust_all') %>%
      .[, fc_clust := new_clust ] %>% .[, new_clust := NULL] %>%
      .[ order(fc_clust, gene_id, lesion_type) ] %>%
      .[, cluster_id := cl]

    return(dt_melt)
    }) %>% rbindlist %>%
    .[, cluster_id := factor(cluster_id, levels = broad_ord) %>% fct_drop ]

  return(fc_cl_dt)
}

plot_de_barplot_sel <- function(run_spec, padj_cut = NULL, fc_cut = NULL, 
  facet_by = c('cluster_id', 'test_var')) {
  # check inputs
  facet_by    = match.arg(facet_by)

  # unpack
  run_tag     = run_spec$run_tag
  time_stamp  = run_spec$time_stamp
  sel_cl      = run_spec$sel_cl

  # define relevant files 
  model_dir   = file.path('output/ms10_muscat', run_tag)
  muscat_f    = '%s/muscat_res_dt_%s_%s.txt.gz' %>%
    sprintf(model_dir, run_tag, time_stamp)
  params_f  = '%s/muscat_params_%s_%s.rds' %>%
    sprintf(model_dir, run_tag, time_stamp)

  # load things
  params      = params_f %>% readRDS
  pb          = readRDS(params$pb_f) %>% 
    .subset_pb(params$subset_spec)
  labels_dt   = .load_labels_dt(labels_f, params$cluster_var)
  tfs_dt      = .load_tfs_dt(tfs_f, pb)
  res_dt      = muscat_f %>% fread %>%
    .load_muscat_results(labels_dt, params)

  # adjust signif calls
  res_dt      = .adjust_signif_calls(res_dt, params, padj_cut, fc_cut)

  # restrict to relevant celltypes
  signif_dt   = res_dt[ updown_soup %in% c('up', 'down') ]
  signif_dt   = signif_dt[ type_broad %in% sel_cl ] %>% 
    .[, cluster_id := fct_drop(cluster_id) ]
  uniques_dt  = .calc_uniques_dt(signif_dt, params)
  stacked_dt  = .calc_stacked_dt(uniques_dt) %>%
    .[ unique_lab == 'unique to coef and celltype', 
      unique_lab := 'unique to lesion type and celltype' ] %>%
    .[, unique_lab := factor(unique_lab) ]

  # plot
  what        = "test"
  g           = plot_deg_barplot(stacked_dt[ var_type == what ], 
      signif_dt[ var_type == what ], tfs_dt, 
      facet_by = facet_by, show_tfs = FALSE) +
    labs(y = 'lesion type') + theme(strip.background = element_blank())

  return(g)
}

.adjust_signif_calls <- function(res_dt, params, padj_cut, fc_cut) {
  # adjust signif calls if necessary
  if ( is.null(padj_cut) & is.null(fc_cut) )
    return(res_dt)

  # tweak if only one used
  if (is.null(padj_cut))
    padj_cut  = params$padj_cut
  if (is.null(fc_cut))
    fc_cut    = params$fc_cut

  # udpate calls
  res_dt[, updown_soup := ifelse(
      abs(logFC) > fc_cut, 
      ifelse(
        p_adj.soup < padj_cut, 
        ifelse(logFC > 0, 'up', 'down'), 
        'insignif'),
      'insignif')]

  return(res_dt)  
}

make_deg_tables <- function(run_spec, n_top, labels_f, magma_f, tfs_f, padj_cut = NULL, fc_cut = NULL) {
  # unpack
  run_tag     = run_spec$run_tag
  time_stamp  = run_spec$de_stamp

  # define relevant files 
  model_dir   = file.path('output/ms10_muscat', run_tag)
  muscat_f    = '%s/muscat_res_dt_%s_%s.txt.gz' %>%
    sprintf(model_dir, run_tag, time_stamp)
  params_f  = '%s/muscat_params_%s_%s.rds' %>%
    sprintf(model_dir, run_tag, time_stamp)

  # load things
  params      = params_f %>% readRDS
  pb          = readRDS(params$pb_f) %>% 
    .subset_pb(params$subset_spec)
  labels_dt   = .load_labels_dt(labels_f, params$cluster_var)
  tfs_dt      = .load_tfs_dt(tfs_f, pb)
  res_all     = muscat_f %>% fread %>%
    .load_muscat_results(labels_dt, params)

  # # adjust signif calls
  # res_dt      = .adjust_signif_calls(res_dt, params, padj_cut, fc_cut)

  # restrict to relevant celltypes
  signif_dt   = res_all[ updown_soup %in% c('up', 'down') ]
  uniques_dt  = .calc_uniques_dt(signif_dt, params) %>%
    .[, .(cluster_id, gene_id, unique_var)] %>% unique
  assert_that( all( uniques_dt[, .N, by = .(cluster_id, gene_id)]$N == 1 ) )

  # get external results
  magma_dt    = .load_magma_dt(magma_f, pb) %>%
    .[, .(gene_id, magma_fdr = p_magma_adj)]
  tfs_dt      = .load_tfs_dt(tfs_f, pb)

  # get top genes
  top_dt      = signif_dt[ updown_soup %in% c('up', 'down') ] %>%
    .[ (var_type == 'test') & (coef != "(Intercept)") ] %>%
    .[, .(min_p = min(p_adj.soup)), by = .(cluster_id, gene_id, 
      deg_updown = factor(updown_soup, levels = c('up', 'down'))) ] %>%
    .[ order(cluster_id, deg_updown, min_p) ] %>%
    .[, deg_rank := 1:.N, by = .(cluster_id, deg_updown) ] %>%
    .[ deg_rank <= n_top ]

  # define vars we want
  dcast_frm   = paste0("type_broad + deg_updown + deg_rank + ",
    "gene_id + symbol + gene_biotype + is_tf + magma_fdr + ",
    "specificity + log10CPM + mean_ambient ~ test_var")

  # make table
  deg_dt      = res_all %>% 
    merge(top_dt, by = c('cluster_id', 'gene_id')) %>%
    merge(uniques_dt, by = c('cluster_id', 'gene_id'), all.x = TRUE) %>%
    merge(magma_dt, by = 'gene_id', all.x = TRUE) %>%
    merge(tfs_dt, by = 'gene_id', all.x = TRUE) %>%
    .[ is.na(is_tf), is_tf := FALSE ] %>%
    setnames('updown_soup', 'updown') %>%
    .[, .(
      type_broad = factor(cluster_id, levels = broad_ord), deg_updown, deg_rank,
      gene_id, symbol, gene_biotype, is_tf, magma_fdr, mean_ambient = mean_soup,
      test_var, specificity = unique_var, 
      log10CPM = logCPM / log(10), 
      log2FC = logFC / log(2), log10_fdr = log10(p_adj.soup))] %>%
    dcast.data.table(as.formula(dcast_frm), value.var = c('log10_fdr', 'log2FC')) %>%
    .[, type_broad := type_broad %>% 
      fct_relabel(~ str_replace_all(.x, "/", "and"))]

  return(deg_dt)
}

make_meta_summary <- function(meta_f, olg_grps_f, labelled_f, qc_dir, qc_f) {
  # get metadata
  meta_dt     = load_meta_dt_from_xls(meta_f)
  olg_grps_dt = olg_grps_f %>% fread %>% 
    .[, oligo_grp := factor(oligo_grp, levels = olg_grp_order)]
  meta_dt     = merge(meta_dt, olg_grps_dt, by = 'subject_id', all.x = TRUE)

  # get conos values
  conos_dt    = fread(labelled_f) %>%
    .[, .(sample_id, cell_id)]

  # get QC median values
  by_vars   = c('sample_id')
  qc_stats  = get_qc_dt(qc_dir, qc_f) %>% 
    merge(conos_dt, by = c('cell_id', 'sample_id')) %>%
    .[, c(lapply(.SD, median), N_sample = .N * 1), by = by_vars, 
      .SDcols = setdiff(names(.), c('cell_id', by_vars))] %>%
    .[, .(sample_id, N_sample, 
      counts        = round(10^log_counts), 
      feats         = round(10^log_feats), 
      pct_mito      = plogis(logit_mito) * 100, 
      pct_unspliced = 1/(1 + 2^splice_ratio) * 1e2
      ) ]

  # join all together
  meta_summ   = merge(meta_dt, qc_stats, by = 'sample_id') %>%
    .[ order(sample_id) ]

  return(meta_summ)
}

make_deg_legend <- function() {
  legend_ls = c(
    type_broad    = 'broad celltype',
    deg_updown    = 'differential expressed in which direction?',
    deg_rank      = 'gene ranked by minimum FDR in lesions (split up and down)',
    gene_id       = 'HGNC gene symbol + ensembl ID',
    symbol        = 'HGNC gene symbol',
    gene_biotype  = 'gene biotype (pseudogenes excluded)',
    is_tf         = 'is the gene a transcription factor?',
    magma_fdr     = paste0('Benjamini-Hochberg adjusted p-value of ',
      'GWAS association calculated by MAGMA (NA indicates either that ',
      'no GWAS SNPs were near the gene, ',
      'or that the gene is not protein-coding'),
    specificity   = paste0('indicates whether the gene is DE only for this ',
      'combination of lesion and cell type, only DE for this cell type ',
      '(i.e. is DE in multiple lesion types for this cell type), ',
      'or are also DE in another cell type (theoretically this can mean that ',
      'the gene is only DE in one lesion type, but in practice there are ',
      'very few such genes)'),
    log10CPM      = 'mean log10(CPM + 1) expression of the gene',
    mean_ambient  = paste0('mean proportion of ambient reads for this gene, ',
      'estimated by DropletUtils, mean taken across samples with non-zero ',
      'expression'),
    log10_fdr_X   = paste0('log10 Benjamini-Hochberg adjusted p-value of ',
      'model coefficient X'),
    log2FC_X      = paste0('log2 fold change of model coefficient X relative ',
      'to reference (i.e. ctrl WM/GM, female, average age, low PMI')
  )
  legend_dt   = data.table(
    column      = names(legend_ls),
    description = legend_ls
    )
  return(legend_dt)
}

make_meta_legend <- function() {
  legend_ls = c(
    sample_id     = 'sample label',
    subject_id    = 'donor ID',
    tissue_id     = 'tissue block ID',
    lesion_type   = 'lesion label (WM = ctrl WM, GM = ctrl GM)',
    seq_pool      = 'sequencing batch',
    matter        = 'white matter or grey matter',
    sample_source = 'brain bank of origin',
    diagnosis     = 'MS diagnosis',
    sex           = 'male or female (confirmed by genotyping)',
    age_at_death  = 'age at death in years',
    years_w_ms    = 'number of years from diagnosis with MS to death',
    pmi_minutes   = 'post-mortem interval in minutes',
    smoker        = 'smoking status, where known',
    age_scale     = 'age at death, scaled to have mean 0 and SD 0.5 over all donors',
    age_cat       = 'age, categorised',
    pmi_cat       = 'PMI, categorised',
    pmi_cat2      = 'PMI, categorised in larger groups for GM',
    oligo_grp     = 'oligodendroglia compositional grouping (by donor)',
    N_sample      = 'number of QC-passing cells in sample',
    counts        = 'median counts in QC-passing cells in sample',
    feats         = 'median genes expressed in QC-passing cells in sample',
    pct_mito      = 'median percentage mitochondrial reads in QC-passing cells in sample',
    pct_unspliced = 'median percentage unspliced reads in QC-passing cells in sample'
  )
  legend_dt   = data.table(
    column      = names(legend_ls),
    description = legend_ls
    )
  return(legend_dt)
}

plot_deg_barplot <- function(stacked_dt, signif_dt, tfs_dt,
  facet_by = c('test_var', 'cluster_id'), show_tfs = TRUE) {
  # check inputs
  facet_by      = match.arg(facet_by)

  # some things for plotting
  updown_cols   = c(nice_cols[1], nice_cols[3]) %>% setNames(c('up', 'down'))
  alpha_vals    = c(
    `shared with other celltype`          = 0.1,
    `shared within broad celltype`        = 0.5,
    `shared within fine celltype`         = 0.7,
    `unique to lesion type and celltype`  = 1
  )
  alpha_vals    = alpha_vals[ levels(fct_drop(stacked_dt$unique_lab)) ]
  
  # decide how to split
  if (facet_by == 'test_var') {
    y_var     = 'cluster_id'
    y_lab     = 'cluster_id'
    facet_var = 'test_var'
  } else if (facet_by == 'cluster_id') {
    y_var     = 'test_var'
    y_lab     = 'model coefficient'
    facet_var = 'cluster_id'
  }

  # define limits
  N_totals    = stacked_dt[, .(N_total = sum(abs(N))), 
      by = .(cluster_id, test_var, updown_soup)] %>% use_series('N_total')
  x_max       = ceiling(max(N_totals / 100)) * 100

  # define TF annotations
  tf_counts   = signif_dt[ gene_id %in% tfs_dt$gene_id ] %>%
    .[, .N, by = .(cluster_id, test_var = fct_drop(test_var), 
        updown_soup = factor(updown_soup, levels = c('up', 'down')))] %>% 
    dcast.data.table(cluster_id + test_var ~ updown_soup, 
      drop = FALSE, value.var = 'N', fill = 0) %>%
    .[up & is.na(down),  tf_label := "TFs: none" ] %>%
    .[, tf_label := sprintf('TFs: %d up, %d down', up, down) ]

  # tweak what to plot
  plot_dt   = copy(stacked_dt) %>% .[, test_var := fct_drop(test_var)]
  if (all(levels(stacked_dt$cluster_id) %in% names(broad_short)))
    plot_dt[, cluster_id := broad_short[ as.character(cluster_id) ] %>% 
      factor(levels = unlist(broad_short)) %>% fct_drop ]

  # make plot
  g = ggplot(plot_dt) + 
    aes_string(y = y_var, x = 'N') +
    geom_vline( xintercept = 0, colour = 'grey' ) +
    geom_col( aes(alpha = unique_lab, colour = updown_soup, fill = updown_soup) ) +
    scale_x_continuous(breaks = pretty_breaks(), labels = abs) +
    scale_y_discrete( limits = rev ) +
    scale_colour_manual(values = updown_cols, guide = 'none') +
    scale_fill_manual(values = updown_cols, guide = 'none') +
    scale_alpha_manual(values = alpha_vals, 
      guide = guide_legend(reverse = TRUE)) +
    coord_cartesian(xlim = c(-x_max, x_max)) +
    theme_bw() + 
    theme( legend.position = "bottom", legend.direction = "vertical",
      panel.grid = element_blank() ) +
    labs(y = y_lab, x = 'no. significant genes', 
      alpha = 'Is result unique?')

  # decide how to facet
  if (facet_by == 'test_var') {
    g = g + facet_grid( . ~ test_var)
  } else if (facet_by == 'cluster_id') {
    g = g + facet_grid( cluster_id ~ .)
  }

  # should we overlay with counts of TFs?
  if (show_tfs)
    g = g + geom_text(data = tf_counts, x = x_max, aes(label = tf_label), 
      hjust = 1, size = 2)

  return(g)
}

plot_causes_of_variability <- function(causes_dt) {
  # count them
  plot_dt   = causes_dt[, .N, by = .(cluster_id, cause)] %>%
    .[, prop := N / sum(N), by = cluster_id ] %>%
    .[ order(cluster_id, cause) ]

  # define colours
  cause_cols  = c(
    `MS effect only` = '#b2182b',
    `both` = '#998ec3',
    `donor effect only` = '#2166ac',
    `none found` = 'grey90'
    )

  # plot
  g = ggplot(plot_dt[ cause != 'none found' ]) +
    aes( x = fct_rev(cluster_id), y = prop, fill = fct_rev(cause) ) +
    geom_col() +
    scale_y_continuous( breaks = pretty_breaks(), 
      labels = percent_format(accuracy = 1L) ) +
    scale_fill_manual( values = cause_cols ) +
    coord_flip() + 
    theme_bw() + theme( panel.grid = element_blank() ) +
    labs( y = 'Proportion of genes', x = 'Broad celltype', 
      fill = 'gene\nclassification')

  return(g)
}

plot_fc_cluster_profiles <- function(fc_cl_dt) {
  # check for what to plot
  plot_dt   = fc_cl_dt[ !is.na(fc_clust) ]
  cl_ls     = plot_dt$cluster_id %>% fct_drop %>% levels

  # how many panels do we need?
  cl_ns     = plot_dt[, .(cluster_id, fc_clust)] %>% unique %>% 
    .[, .N, by = cluster_id]
  n_panels  = cl_ns$N %>% max

  # plot for each cluster, join together
  g_list    = lapply(cl_ls, .plot_logfc_clusters_one_cl, plot_dt, n_panels)
  g         = wrap_plots(g_list, ncol = 1)

  return(g)
}

.plot_logfc_clusters_one_cl <- function(cl, plot_dt, n_panels) {
  # restrict to this cluster and exclude small clusters
  tmp_dt    = plot_dt[ cluster_id == cl ] %>%
    .[, cl_lab := sprintf("Pattern %d (%d)", fc_clust, n_clust), 
      by = fc_clust ] %>%
    .[, cl_lab := cl_lab %>% factor %>% fct_reorder(as.integer(fc_clust)) ]

  # calculate mean profiles
  tmp_stats = tmp_dt[, .(log2FC = logFC / log(2), cl_lab, lesion_type) ] %>%
    .[, .(
      med       = median(log2FC),
      q05       = quantile(log2FC, prob = 0.05),
      q95       = quantile(log2FC, prob = 0.95)
      ), by = .(cl_lab, lesion_type) ]

  # how many spacers?
  n_used    = length(unique(tmp_dt$fc_clust))
  n_not     = n_panels - n_used

  # do plot
  g_plot = ggplot( tmp_stats ) + 
    aes( x = lesion_type ) +
    geom_hline( yintercept = 0, colour = 'black', linetype = 'dashed' ) +
    # geom_line( data = tmp_dt, aes(group = gene_id, y = logFC / log(2)), 
    #   colour = 'grey', alpha = 0.2, position = position_jitter( height = 0, width = 0.5) ) + 
    geom_ribbon( aes( ymin = q05, ymax = q95 ), group = 1,
      fill = 'grey', alpha = 0.5 ) + 
    geom_line( data = tmp_stats, aes( y = med ), 
      colour = 'black', group = 1 ) + 
    scale_y_continuous( breaks = pretty_breaks() ) +
    coord_cartesian( ylim = c(-4, 4) ) +
    facet_wrap( ~ cl_lab, nrow = 1 ) +
    theme_bw() + 
    theme( panel.grid = element_blank(), axis.text.x = element_text( size = 6 ),
      strip.text = element_text(size = 8, margin = margin(0.02, 0.02, 0.02, 0.02,  "in")
      ) ) +
    labs( x = NULL, y = sprintf("log2FC in\n%s", broad_short[[cl]]) )

  # add spacers if necessary
  g   = g_plot + plot_spacer() + 
    plot_layout( widths = c(n_used, n_not ) )

  return(g)
}

plot_top_genes_across_celltypes <- function(run_spec, updown = c('up', 'down'),
  rank_var = c("rank_fc", "rank_p"), n_top = 5, max_fc = log(16), 
  padj_cut = NULL, fc_cut = NULL) {
  # check
  updown      = match.arg(updown)
  rank_var    = match.arg(rank_var)

  # unpack
  run_tag     = run_spec$run_tag
  time_stamp  = run_spec$time_stamp

  # define relevant files 
  model_dir   = file.path('output/ms10_muscat', run_tag)
  muscat_f    = '%s/muscat_res_dt_%s_%s.txt.gz' %>%
    sprintf(model_dir, run_tag, time_stamp)
  params_f  = '%s/muscat_params_%s_%s.rds' %>%
    sprintf(model_dir, run_tag, time_stamp)

  # load things
  params      = params_f %>% readRDS
  pb          = readRDS(params$pb_f) %>% 
    .subset_pb(params$subset_spec)
  labels_dt   = .load_labels_dt(labels_f, params$cluster_var)
  tfs_dt      = .load_tfs_dt(tfs_f, pb)
  magma_dt    = .load_magma_dt(magma_f, pb)
  lof_dt      = .load_lof_dt(lof_f, pb)

  # get muscat results, adjust if necessary
  res_dt      = muscat_f %>% fread %>%
    .load_muscat_results(labels_dt, params)
  res_dt      = .adjust_signif_calls(res_dt, params, padj_cut, fc_cut)

  # restrict to signif genes in relevant celltypes
  signif_dt   = res_dt[ updown_soup != 'insignif' & !is.na(p_adj.soup) ]
  signif_dt   = signif_dt[ cluster_id %in% run_spec$sel_cl ]
  uniques_dt  = .calc_uniques_dt(signif_dt, params) %>%
    .[, unique_var := unique_var %>%
      fct_collapse(
        `this type only`  = c('lesion_specific', 'broad_specific'),
        `shared`          = c('non_specific')
      ) %>% fct_drop ]
  stacked_dt  = .calc_stacked_dt(uniques_dt)

  # get top genes for each celltype
  ranks_dt    = uniques_dt[ var_type == "test" ] %>%
    .[, .(cluster_id, var_type, test_var, gene_id, unique_var)] %>%
    merge(signif_dt, , by = c('cluster_id', 'var_type', 'test_var', 'gene_id')) %>%
    .[ (cluster_id %in% run_spec$sel_cl) & (gene_biotype %in% c("protein_coding", "IG_C_gene")) ] %>%
    .[ updown_soup == ud ] %>%
    .[abs(logFC) > params$fc_cut, .(
      max_logfc = max(abs(logFC)), 
      min_padj  = min(p_adj.soup)
      ), by = .(gene_id, cluster_id, unique_var, var_type)] %>%
    .[, rank_fc := frank(-max_logfc, ties.method = 'random'), 
      by = cluster_id ] %>%
    .[, rank_p  := frank(min_padj, ties.method = 'random'), 
      by = cluster_id ]

  # get values for these genes
  keep_idx    = ranks_dt[[ rank_var ]] <= n_top
  top_dt      = ranks_dt[ keep_idx, 
      .(var_type, gene_id, cluster_id, unique_var)] %>% 
    unique %>%
    merge(res_dt, by = c('var_type', 'gene_id', 'cluster_id'), all = FALSE) %>%
    merge(magma_dt, by = 'gene_id', all.x = TRUE) %>%
    merge(tfs_dt, by = 'gene_id', all.x = TRUE) %>%
    .[is.na(is_tf), is_tf := FALSE ] %>%
    merge(lof_dt, by = 'gene_id', all.x = TRUE)
    # merge(coloc_dt, by = c('type_broad', 'gene_id'), all.x = TRUE)

  # make heatmap
  hm_obj      = plot_top_genes_heatmap_multiple_celltypes_fn(top_dt, labels_dt, max_fc = max_fc)

  return(hm_obj)
}

plot_top_genes_heatmap_multiple_celltypes_fn <- function(input_dt, labels_dt, 
  row_split = NULL, max_fc = log(16)) {
  # make mat of FCs
  log2fc_wide  = copy(input_dt) %>%
    .[, log2fc_trunc  := pmin(abs(logFC), max_fc) * sign(logFC) / log(2) ] %>%
    .[, cn            := paste0(broad_short[cluster_id], '.', test_var) ] %>%
    .[ order(cluster_id, test_var) ] %>% .[, cn := fct_inorder(cn)] %>%
    dcast.data.table(symbol ~ cn, value.var = 'log2fc_trunc', fill = 0)
  log2fc_mat   = log2fc_wide %>% as.matrix(rownames = 'symbol')

  # get p-value annotations
  p_wide      = copy(input_dt) %>%
    .[, p     := p_adj.soup] %>%
    .[, star  := ifelse(
      p < 0.001, '***', ifelse(
        p < 0.01, '**', ifelse(
          p < 0.05, '*', ifelse(
            p < 0.1, '.', ''
        ))))] %>% 
    .[, cn            := paste0(broad_short[cluster_id], '.', test_var) ] %>%
    .[ order(cluster_id, test_var) ] %>% .[, cn := fct_inorder(cn)] %>%
    dcast.data.table(symbol ~ cn, value.var = 'star', fill = '')
  p_mat       = p_wide %>% as.matrix(rownames = 'symbol')
  p_fn <- function(j, i, x, y, width, height, fill) {
    grid.text(sprintf("%s", p_mat[i, j]), x, y, 
      gp = gpar(fontsize = 8))
  }

  # # make row annotations
  # rows_dt   = input_dt[, .(symbol, unique_var)] %>%
  #   .[ unique_var != 'not_signif' ] %>%
  #   unique %>% setkey('symbol') %>%
  #   .[ rownames(log2fc_mat) ]

  # some colours
  # unique_labs = c(
  #   lesion_specific = 'unique to coef and celltype', 
  #   broad_specific  = 'shared within broad celltype', 
  #   fine_specific   = 'shared within fine celltype', 
  #   non_specific    = 'shared with other celltype'
  # )
  # unique_labs = unique_labs[ unique(rows_dt[ !is.na(unique_var) ]$unique_var) ]
  # unique_cols = brewer.pal(length(unique_labs), 'Greys') %>% rev %>% 
  #   setNames(unique_labs)
  max_log2fc  = max_fc / log(2)
  fc_cols     = cols_fn(seq(-max_log2fc, max_log2fc), 1, 'RdBu', 
    pal_dir=-1, range='symmetric')

  # # make row annotations
  # row_annots  = rowAnnotation(
  #   specificity = unique_labs[rows_dt$unique_var], 
  #   col     = list(
  #     specificity = unique_cols
  #   ),
  #   annotation_name_side='top'
  # )

  # check that the row split option is ok
  if (!is.null(row_split)) {
    # assert_that(all( rownames(log2fc_mat) %in% unlist(row_split) ))
    split_dt    = lapply(names(row_split), 
        function(n) data.table( grp = n, symbol = row_split[[n]] )) %>% rbindlist %>%
      .[, grp := grp %>% factor(levels = names(row_split)) ] %>%
      setkey('symbol') %>% .[ rownames(log2fc_mat) ]
  }

  # split columns
  short_lu    = names(broad_short) %>% setNames(broad_short)
  col_split   = colnames(log2fc_mat) %>% str_extract('^[^\\.]+') %>% 
    factor(levels = broad_short)
  col_labels  = colnames(log2fc_mat) %>% str_extract('[^\\.]+$')
  row_labels  = rownames(log2fc_mat) %>% str_extract('^[^_]+')

  # do heatmap for these genes
  name_str  = paste0("log2FC rel.\nto reference\n(capped at +/-", max_log2fc, ')')
  hm_obj    = Heatmap(
    matrix = log2fc_mat, col = fc_cols, cell_fun = p_fn,
    name = 'log2FC',
    cluster_rows = TRUE, cluster_columns = FALSE,
    row_names_gp = gpar(fontsize = 8), row_names_side = "left",
    row_split = split_dt$grp, cluster_row_slices = FALSE,
    row_title_gp = gpar(fontsize = 10), 
    column_names_gp = gpar(fontsize = 10), column_names_side = "top",
    column_split = col_split, cluster_column_slices = FALSE,
    column_labels = col_labels,
    # left_annotation = row_annots,
    heatmap_legend_param = list(title = name_str)
  )

  return(hm_obj)
}

plot_ifn_genes_heatmap <- function(sel_dt, labels_dt, ifn_gs_dt, 
  max_fc = log(16), title='', magma_cut = 0.05) {
  # make mat of FCs
  logfc_wide  = copy(sel_dt) %>%
    .[, log2fc_trunc  := pmin(abs(logFC), max_fc) * sign(logFC) / log(2) ] %>%
    .[, short_cl      := broad_short[cluster_id], by = cluster_id ] %>%
    dcast.data.table(
      symbol + p_magma_adj + is_tf ~ short_cl + test_var, 
      value.var = 'log2fc_trunc', fill = 0, sep = '.')
  log2fc_mat  = logfc_wide[, -c('symbol', 'p_magma_adj', 'is_tf'), with = FALSE] %>%
    as.matrix %>% set_rownames(logfc_wide$symbol)

  # do column split
  short_broad = names(broad_short) %>% setNames(broad_short)
  col_split   = str_extract(colnames(log2fc_mat), "^[^\\.]+") %>%
    short_broad[ . ] %>% factor(levels = broad_ord)
  col_names   = str_extract(colnames(log2fc_mat), "[^\\.]+$")

  # get p-value annotations
  p_wide      = copy(sel_dt) %>%
    .[, short_cl      := broad_short[cluster_id], by = cluster_id ] %>%
    .[, p     := p_adj.soup] %>%
    .[, star  := ifelse(
      p < 0.001, '***', ifelse(
        p < 0.01, '**', ifelse(
          p < 0.05, '*', ifelse(
            p < 0.1, '.', ''
        ))))] %>% 
    dcast.data.table(symbol ~ short_cl + test_var, value.var = 'star', 
      fill = '', sep = ".")
  p_mat       = as.matrix(p_wide[, !'symbol']) %>% 
    set_rownames(p_wide$symbol)
  p_fn <- function(j, i, x, y, width, height, fill) {
    grid.text(sprintf("%s", p_mat[i, j]), x, y, 
      gp = gpar(fontsize = 10))
  }

  # make row annotations
  rows_dt   = copy(ifn_gs_dt) %>%
    setkey('symbol') %>%
    .[rownames(log2fc_mat)]

  # annotate gene names with magma signif
  magma_vals  = logfc_wide$p_magma_adj
  magma_vals[ is.na(magma_vals) ] = 1
  signif_idx  = magma_vals < magma_cut
  row_labs    = rownames(log2fc_mat)
  if (any(signif_idx))
    row_labs[ signif_idx ] = paste0(row_labs[ signif_idx ], "*")

  # make row annotations
  bool_cols   = structure(c('white', 'black'), names = c("0", "1"))
  # bool_cols   = c(`TRUE` = 'black', `FALSE` = 'white')
  # row_annots  = rowAnnotation(
  #   `Type i`    = rows_dt$type_i, 
  #   `Type ii`   = rows_dt$type_ii, 
  #   col     = list(
  #     `Type i`    = bool_cols,
  #     `Type ii`   = bool_cols
  #   ),
  #   annotation_name_side='top', show_legend = FALSE
  # )
  row_annots  = rowAnnotation(
    ifn_alpha   = rows_dt$alpha, 
    ifn_gamma   = rows_dt$gamma, 
    col     = list(
      ifn_alpha   = bool_cols,
      ifn_gamma   = bool_cols
    ),
    annotation_name_side='top', show_legend = FALSE
  )

  # get nice cols
  max_log2fc  = max_fc / log(2)
  fc_cols     = cols_fn(seq(-max_log2fc, max_log2fc), 1, 'RdBu', 
    pal_dir=-1, range='symmetric')

  # do heatmap for these genes
  name_str  = paste0("log2FC rel.\nto reference\n(capped at +/-", max_log2fc, ')')
  hm_obj    = Heatmap(
    matrix = log2fc_mat, col = fc_cols, cell_fun = p_fn,
    cluster_rows = TRUE, cluster_columns = FALSE,
    row_names_gp = gpar(fontsize = 10), 
    column_names_gp = gpar(fontsize = 10),
    column_title_gp = gpar(fontsize = 10),
    name = 'log2fc',
    left_annotation = row_annots,
    row_names_side="left", column_names_side="top",
    column_title = title, 
    column_split = col_split, cluster_column_slices = FALSE,
    column_labels = col_names, row_labels = row_labs,
    heatmap_legend_param = list(
      title = name_str, at = c(-max_log2fc, 0, max_log2fc),
      direction = "horizontal", title_position = "lefttop"
    ))

  return(hm_obj)
}

plot_neuron_genes_heatmap_fn <- function(input_dt, labels_dt, max_fc = log(16), 
  title='', row_split = NULL) {
  # make mat of FCs
  logfc_wide  = copy(input_dt) %>%
    .[, log2fc_trunc  := pmin(abs(logFC), max_fc) * sign(logFC) / log(2) ] %>%
    .[, log2CPM       := logCPM / log(2) ] %>%
    dcast.data.table(
      symbol + unique_var + log2CPM + p_magma_adj + p_coloc + is_tf ~ test_var, 
      value.var = 'log2fc_trunc', fill = 0) %>%
    .[, log2CPM := pmax(log2CPM, 0)]
  logfc_mat   = logfc_wide[, -c('symbol', 'unique_var', 'log2CPM', 
    'p_magma_adj', 'p_coloc', 'is_tf'), with = FALSE] %>%
    as.matrix %>% set_rownames(logfc_wide$symbol)

  # check that the row split option is ok
  if (!is.null(row_split)) {
    assert_that(all( rownames(logfc_mat) %in% names(row_split) ))
    # now put in same order
    row_split   = row_split[ rownames(logfc_mat) ]
  }

  # get p-value annotations
  p_wide      = copy(input_dt) %>%
    .[, p     := p_adj.soup] %>%
    .[, star  := ifelse(
      p < 0.001, '***', ifelse(
        p < 0.01, '**', ifelse(
          p < 0.05, '*', ifelse(
            p < 0.1, '.', ''
        ))))] %>% 
    dcast.data.table(symbol ~ test_var, value.var = 'star', fill = '')
  p_mat       = as.matrix(p_wide[, !'symbol']) %>% 
    set_rownames(p_wide$symbol)
  p_fn <- function(j, i, x, y, width, height, fill) {
    grid.text(sprintf("%s", p_mat[i, j]), x, y, 
      gp = gpar(fontsize = 8))
  }

  # get nice cols
  max_log2fc  = max_fc / log(2)
  fc_cols     = cols_fn(seq(-max_log2fc, max_log2fc), 1, 'RdBu', 
    pal_dir=-1, range='symmetric')

  # do heatmap for these genes
  name_str  = paste0("log2FC rel.\nto reference\n(capped at +/-", max_log2fc, ')')
  hm_obj    = Heatmap(
    matrix=logfc_mat, col=fc_cols, cell_fun = p_fn,
    cluster_rows=TRUE, cluster_columns=FALSE,
    row_names_gp=gpar(fontsize = 8), column_names_gp=gpar(fontsize = 10),
    name = 'log2fc',
    row_names_side="left",
    column_names_side="top",
    column_title = title, 
    row_split = row_split, cluster_row_slices = FALSE,
    row_title_gp = gpar(fontsize = 8),
    heatmap_legend_param = list(
      title = name_str, at = c(-max_log2fc, 0, max_log2fc)
    ))

  return(hm_obj)
}

rerun_fgsea <- function() {
  # define source files
  broad_spec  = list(
    run_tag       = 'run23',
    time_stamp    = '2021-11-15'
  )
  # broad_spec  = list(
  #   run_tag     = 'run09',
  #   time_stamp  = '2021-10-13'
  # )
  run_tag     = broad_spec$run_tag
  time_stamp  = broad_spec$time_stamp

  # define files
  model_dir   = file.path('output/ms10_muscat', run_tag)
  params_f    = '%s/muscat_params_%s_%s.rds' %>%
    sprintf(model_dir, run_tag, time_stamp)
  res_all_f   = '%s/muscat_res_dt_%s_%s.txt.gz' %>%
    sprintf(model_dir, run_tag, time_stamp)

  # define alternative fgsea_pat
  # fgsea_pat   = sprintf('%s/fgsea_dt_z_%s_%s_%s.txt.gz', 
  fgsea_pat   = sprintf('%s/fgsea_dt_%s_%s_%s.txt.gz', 
    model_dir, run_tag, '%s', time_stamp)

  # load muscat outputs
  params      = readRDS(params_f)
  res_all     = fread(res_all_f) %>% 
    .[, .(cluster_id, coef, symbol, logFC, p_val, p_adj.soup)]

  # check what needs to be done
  schon_done  = sprintf(fgsea_pat, names(paths_list)) %>% file.exists
  paths_list  = paths_list[ !schon_done ]
  
  # set up cluster
  n_cores     = 8
  bpparam     = MulticoreParam(workers = n_cores, progressbar = TRUE, tasks = 50)
  bpstop()
  bpstart()

  # call w z
  .calc_fgsea_dt(paths_list, res_all, list(fgsea_pat = fgsea_pat), 
    params$fgsea_cut, time_stamp, bpparam, order_var = 'z')
  bpstop()
}
