#!/usr/bin/env python
# coding: utf-8
# python ./code/ms08_modules.py

# ms08_popalign.py

import os
import re
import popalign as PA
import pickle
import numpy as np
from scipy import io as sio

def run_onmf(save_dir, date_tag, group_list, ncores = 8):
  # import pdb; pdb.set_trace()

  # define some files
  mtx_re      = f'^(counts_{date_tag}_)(.+)(.mtx)'

  # loop through each
  for g in group_list:
    # define file to make
    sub_dir   = os.path.join(save_dir, g)
    pop_f     = os.path.join(sub_dir, f'pop_{g}_{date_tag}.p')
    if os.path.isfile(pop_f):
      continue
    print(g)

    # what to load?
    mtx_list    = [f for f in os.listdir(sub_dir) if re.search(mtx_re, f)]
    types_list  = [re.match(mtx_re, f)[2] for f in mtx_list]
    samples     = dict(zip(types_list, 
      [os.path.join(sub_dir, f) for f in mtx_list]))

    print('  loading samples')
    features_f  = os.path.join(sub_dir, f'features_{date_tag}_{g}.tsv')
    pop         = PA.load_samples(samples=samples, genes=features_f, 
      outputfolder=sub_dir)
    pop['ncores'] = ncores
    
    print('  normalizing')
    PA.normalize(pop)
    print('  identifying HVGs')
    PA.plot_gene_filter(pop, offset=0.1)
    PA.filter(pop, remove_ribsomal=False, remove_mitochondrial=False)
    
    print('  running oNMF')
    PA.onmf(pop, ncells=5000, nfeats=np.arange(2,20,2).tolist(), nreps=3, 
      niter=200)

    print('  running GSEA')
    PA.choose_featureset(pop, alpha=3, multiplier=3)

    # save
    print('  saving')
    pickle.dump(pop, open(pop_f, "wb"))


# if __name__ == '__main__':
#   save_dir    = 'output/ms08_modules'
#   date_tag    = '2021-08-17'
#   group_list  = ["oligo_opc", "micro_immune", "excitatory", "inhibitory", 
#     "astrocytes", "endo_stromal", "microglia", "immune"]
#   run_onmf(save_dir, date_tag, group_list)
