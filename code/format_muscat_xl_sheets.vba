Sub nice_formatting()
  For Each sh In Worksheets()
    sh.Activate
    nice_formatting_one_sheet
  Next
End Sub

Sub nice_formatting_one_sheet()  
  ' define current cells of interest
  Dim rng as Range

  ' find last row
  first_row = 2
  last_row  = ActiveSheet.UsedRange.Rows.Count

  ' freeze first row
  With ActiveWindow
    .SplitColumn = 0
    .SplitRow = 1
  End With
  ActiveWindow.FreezePanes = True

  ' filter
  Rows("1").AutoFilter

  ' widths
  Columns("A:A").ColumnWidth = 16
  Columns("B:B").ColumnWidth = 30
  Columns("C:C").ColumnWidth = 12
  Set rng = Columns("C:C")
  add_border rng

  ' reset any formatting
  Cells.FormatConditions.Delete

  ' format logCPM
  Set rng = Range(Cells(first_row, "D"), Cells(last_row, "D"))
  rng.NumberFormat = "#,##0.00"
  add_border rng
  format_logcpm rng

  ' format soup
  Set rng = Range(Cells(first_row, "E"), Cells(last_row, "E"))
  rng.NumberFormat = "#,##0.00"
  add_border rng
  format_soup rng

  ' format logfc
  Set rng = Range(Cells(first_row, "F"), Cells(last_row, "N"))
  rng.NumberFormat = "#,##0.00"
  add_border rng
  format_logfc rng

  ' format p-values
  Set rng = Range(Cells(first_row, "O"), Cells(last_row, "W"))
  rng.NumberFormat = "0.0E+00"
  add_border rng
  format_pvalues rng
End Sub

Sub format_logcpm(rng As Range):
  ' define colours
  Dim pal_vir1 As Long
  Dim pal_vir2 As Long
  Dim pal_vir3 As Long
  pal_vir1    = RGB(59,28,140)
  pal_vir2    = RGB(33,144,141)
  pal_vir3    = RGB(249,231,33)

  ' do formatting
  rng.FormatConditions.AddColorScale ColorScaleType:=3
  With rng.FormatConditions(1)
    .ColorScaleCriteria(1).Type   = xlConditionValuePercentile
    .ColorScaleCriteria(1).Value  = 5
    .ColorScaleCriteria(1).FormatColor.Color = pal_vir1

    .ColorScaleCriteria(2).Type   = xlConditionValuePercentile
    .ColorScaleCriteria(2).Value  = 50
    .ColorScaleCriteria(2).FormatColor.Color = pal_vir2

    .ColorScaleCriteria(3).Type   = xlConditionValuePercentile
    .ColorScaleCriteria(3).Value  = 95
    .ColorScaleCriteria(3).FormatColor.Color = pal_vir3
  End With
End Sub

Sub format_soup(rng As Range):
  ' define colours
  Dim pal_greens1 As Long
  Dim pal_greens2 As Long
  Dim pal_greens3 As Long
  pal_greens1 = RGB(247,252,253)
  pal_greens2 = RGB(102,194,164)
  pal_greens3 = RGB(0,68,27)

  rng.FormatConditions.AddColorScale ColorScaleType:=3
  With rng.FormatConditions(1)
    .ColorScaleCriteria(1).Type   = xlConditionValueNumber
    .ColorScaleCriteria(1).Value  = 0
    .ColorScaleCriteria(1).FormatColor.Color = pal_greens1

    .ColorScaleCriteria(2).Type   = xlConditionValueNumber
    .ColorScaleCriteria(2).Value  = 0.1
    .ColorScaleCriteria(2).FormatColor.Color = pal_greens2

    .ColorScaleCriteria(3).Type   = xlConditionValueNumber
    .ColorScaleCriteria(3).Value  = 0.2
    .ColorScaleCriteria(3).FormatColor.Color = pal_greens3
  End With
End Sub

Sub format_logfc(rng As Range):
  Dim pal_blue As Long
  Dim pal_white As Long
  Dim pal_red As Long
  pal_blue    = RGB(33, 102, 172)
  pal_white   = RGB(247, 247, 247)
  pal_red     = RGB(178, 24, 43)

  rng.FormatConditions.AddColorScale ColorScaleType:=3
  With rng.FormatConditions(1)
    .ColorScaleCriteria(1).Type   = xlConditionValueNumber
    .ColorScaleCriteria(1).Value  = -3
    .ColorScaleCriteria(1).FormatColor.Color = pal_blue

    .ColorScaleCriteria(2).Type   = xlConditionValueNumber
    .ColorScaleCriteria(2).Value  = 0
    .ColorScaleCriteria(2).FormatColor.Color = pal_white

    .ColorScaleCriteria(3).Type   = xlConditionValueNumber
    .ColorScaleCriteria(3).Value  = 3
    .ColorScaleCriteria(3).FormatColor.Color = pal_red
  End With
End Sub

Sub format_pvalues(rng As Range):
  ' define colours
  Dim pal_purps_1 As Long
  Dim pal_purps_2 As Long
  Dim pal_purps_3 As Long
  pal_purps_1 = RGB(247,252,253)
  pal_purps_2 = RGB(140,150,198)
  pal_purps_3 = RGB(136,65,157)

  ' do formatting
  rng.FormatConditions.AddColorScale ColorScaleType:=3
  With rng.FormatConditions(1)
    .ColorScaleCriteria(1).Type   = xlConditionValueNumber
    .ColorScaleCriteria(1).Value  = 1E-10
    .ColorScaleCriteria(1).FormatColor.Color = pal_purps_3

    .ColorScaleCriteria(2).Type   = xlConditionValueNumber
    .ColorScaleCriteria(2).Value  = 0.05
    .ColorScaleCriteria(2).FormatColor.Color = pal_purps_2

    .ColorScaleCriteria(3).Type   = xlConditionValueNumber
    .ColorScaleCriteria(3).Value  = 0.1
    .ColorScaleCriteria(3).FormatColor.Color = pal_purps_1
  End With
End Sub

Sub add_border(rng As Range):
  rng.Borders(xlEdgeRight).LineStyle = xlThin
End Sub
