# ms10_muscat_slurm_job.R

# get arguments
args = commandArgs(TRUE)
run_tag = args[1]
n_cores = round(as.numeric(args[2]), 0)

# load functions
.libPaths(c("~/lib/conda_r3.12", .libPaths()))
source('code/ms10_muscat_runs.R')

# check arguments
stopifnot(run_tag %in% names(run_descs))
stopifnot(n_cores >= 1)

# do run
do_muscat_run(run_tag, n_cores)
