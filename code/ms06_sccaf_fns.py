# ms16_sccaf_fns.py

print('\nimporting libraries')
import os
import pandas as pd

import scanpy as sc
from SCCAF import SCCAF_assessment

def run_sccaf(adata_f, preds_f, probs_f, n_jobs=8, overwrite=False):
  # import pdb; pdb.set_trace()
  if os.path.isfile(preds_f) and os.path.isfile(probs_f) and not overwrite:
    print('already completed!')
    return

  # load adata object
  print('loading adata object')
  assert os.path.isfile(adata_f)
  adata       = sc.read(adata_f)

  # do sccaf assessment
  print('running sccaf')
  y_prob, y_pred, y_test, clf, cvsm, acc = SCCAF_assessment(adata.X, adata.obs['conos'], n_jobs=n_jobs)

  # save predictions
  print('saving predictions')
  preds_df    = pd.DataFrame({'y_pred': y_pred, 'y_test': y_test})
  preds_df.to_csv(preds_f)

  # save probabilities
  print('saving probabilities')
  probs_df          = pd.DataFrame(y_prob)
  probs_df.index    = preds_df.index
  probs_df.columns  = adata.obs['conos'].cat.categories.tolist()
  probs_df.to_csv(probs_f)
