# ms10_muscat_runs.R
# source('code/ms10_muscat_runs.R')
# source('code/ms10_muscat_runs.R'); do_muscat_run(c('run17'), n_cores = 16)
# source('code/ms10_muscat_runs.R'); do_muscat_run(c('run23'), n_cores = 8)
# source('code/ms10_muscat_runs.R'); render_muscat_results('run09', '2021-10-13')
# source('code/ms10_muscat_runs.R'); render_muscat_results('run10', '2021-10-13')
# source('code/ms10_muscat_runs.R'); render_muscat_results(c('run07', 'run08'), '2021-05-30')
# source('code/ms10_muscat_runs.R'); render_muscat_results(c('run09', 'run10'), '2021-06-17')
# source('code/ms10_muscat_runs.R'); render_muscat_results(c('run11', 'run12'), '2021-06-17')
# source('code/ms10_muscat_runs.R'); render_muscat_results('run23', '2021-11-15')
# source('code/ms10_muscat_runs.R'); do_muscat_run('run11', n_cores = 16);
# source('code/ms10_muscat_runs.R'); bpstop(); do_muscat_run(c('run12'), n_cores = 16);

source('code/ms00_utils.R')
source('code/ms10_muscat_fns.R')
suppressPackageStartupMessages({
  library('rmarkdown')
})

# which runs have we defined?
run_descs   = list(
  run01 = 'lesions vs ctrl, WM, edgeR + treat, broad celltypes', 
  run02 = 'lesions vs ctrl, WM, edgeR + treat, broad celltypes', 
  run03 = 'lesions vs ctrl, WM, limma-random + treat, broad celltypes', 
  run04 = 'lesions vs ctrl, GM, limma-random + treat, broad celltypes', 
  run05 = 'lesions vs ctrl, WM, edgeR + treat, broad celltypes, one random sample per patient', 
  run06 = 'lesions vs ctrl, GM, edgeR + treat, broad celltypes, one random sample per patient', 
  run07 = 'old vs young (>=70 vs <=50), WM MS only, glmmtmb-nb, broad celltypes',
  run08 = 'old vs young (>=70 vs <=50), GM MS only, glmmtmb-nb, broad celltypes',
  run09 = 'lesions vs ctrl, WM, glmmtmb-nb, broad celltypes',
  run10 = 'lesions vs ctrl, GM, glmmtmb-nb, broad celltypes',
  run11 = 'lesions vs ctrl, WM, glmmtmb-nb, fine celltypes',
  run12 = 'lesions vs ctrl, GM, glmmtmb-nb, fine celltypes',
  run13 = 'old vs young (>=70 vs <=50), RL only, glmmtmb-nb, broad celltypes',
  run14 = 'oligo-stress, WM, edgeR, broad celltypes, patients',
  run15 = 'MS status interactions, WM, glmmtmb-nb, broad celltypes',
  run16 = 'MS status interactions, GM, glmmtmb-nb, broad celltypes',
  run17 = 'confounders only, WM, glmmtmb-nb, broad celltypes',
  run18 = 'confounders only, GM, glmmtmb-nb, broad celltypes',
  run19 = 'WM vs GM, broad celltypes',
  run20 = 'WM vs GM, fine celltypes',
  run21 = 'lesions vs ctrl, WM, glmmtmb-nb, broad celltypes, super-clean',
  run22 = 'lesions vs ctrl, GM, glmmtmb-nb, broad celltypes, super-clean',
  run23 = 'lesions vs ctrl w layers, GM, glmmtmb-nb, broad celltypes',
  run24 = 'lesions vs ctrl w layers, GM, glmmtmb-nb, fine celltypes',
  run25 = 'MS WM vs MS GM, broad celltypes'
)

# define pathways
path_dir    = 'data/gmt_pathways'
paths_list  = list(
  go_bp     = 'c5.go.bp.v7.4.symbols.gmt', 
  go_cc     = 'c5.go.cc.v7.4.symbols.gmt', 
  go_mf     = 'c5.go.mf.v7.5.1.symbols.gmt', 
  hallmark  = 'h.all.v7.4.symbols.gmt',
  kegg      = 'c2.cp.kegg.v7.5.1.symbols.gmt',
  TFs       = 'c3.tft.v7.4.symbols.gmt', 
  celltype  = 'c8.all.v7.4.symbols.gmt',
  hpo       = 'c5.hpo.v7.4.symbols.gmt',
  mirs      = 'c3.mir.v7.4.symbols.gmt', 
  curated   = 'c2.all.v7.4.symbols.gmt', 
  immune    = 'c7.immunesigdb.v7.4.symbols.gmt', 
  position  = 'c1.all.v7.4.symbols.gmt'
) %>% lapply(function(p) file.path(path_dir, p))
path_names  = c(
  'GO BP', 
  'GO CC', 
  'GO MF', 
  'Hallmark',
  'KEGG',
  'TFs', 
  'Celltype',
  'Human Phenotype Ontology',
  'MIRs', 
  'Curated', 
  'Immune', 
  'Positional'
) %>% setNames(names(paths_list))
gsea_regex  = '^(HALLMARK_|KEGG_|GO_|GOBP_|GOCC_|GOMF_|HP_|GSE[0-9]+_|GS[0-9]+_|)(.+)'

# define soup directory
soup_dir    = 'output/ms07_soup'
save_dir    = 'output/ms10_muscat'

# define celltype labels
labels_f    = 'data/byhand_markers/validation_markers_2021-05-31.csv'

# define gene annotations
gtf_f       = 'data/gtf/Homo_sapiens.GRCh38.96.filtered.preMRNA.gtf'
tfs_f       = 'data/TFs/Human_TFs_suppl_tables_cell_review_2018.xlsx'
lof_f       = 'data/gnomAD/supplementary_dataset_11_full_constraint_metrics.tsv'

# results from Julien: magma is GWAS, coloc is colocalization (≈ eQTL)
magma_f     = 'data/magma/gene_names_magma.csv'
coloc_f     = 'data/coloc/ms_ad_rosmap_eQTL_coloc.pvalue.ms.txt'

# specify runs
do_muscat_run <- function(run_tags, n_cores = 8) {
  # check inputs
  assert_that(all(run_tags %in% names(run_descs)))
  for (run_tag in run_tags)
    .do_one_muscat_run(run_tag, n_cores)    
}

.do_one_muscat_run <- function(run_tag, n_cores) {
  # define directory
  model_dir   = file.path(save_dir, run_tag)
  if (!dir.exists(model_dir))
    dir.create(model_dir)

  if (run_tag == 'run01') {
    # specify where soup and pseudobulk are located
    pb_soup_f   = file.path(soup_dir, 'pb_soup_broad_maximum_2021-06-01.rds')
    soup_cut    = 0.1

    # specify what goes into muscat run
    cluster_var = 'type_broad'
    pb_f        = file.path(soup_dir, 'pb_sum_broad_2021-06-01.rds')
    formula_str = '~ lesion_type + sex + age_norm + pmi_cat'
    subset_spec = list(matter = 'WM')
    method_spec = list(method = 'edgeR', treat = TRUE)
    padj_cut    = 0.05
    fgsea_cut   = 0.05
    filter      = 'both'
    min_cells   = 10
    output_fs   = .make_output_fs(model_dir, run_tag)

  } else if (run_tag == 'run02') {
    # specify where soup and pseudobulk are located
    pb_soup_f   = file.path(soup_dir, 'pb_soup_broad_maximum_2021-06-01.rds')
    soup_cut    = 0.1

    # specify what goes into muscat run
    cluster_var = 'type_broad'
    pb_f        = file.path(soup_dir, 'pb_sum_broad_2021-06-01.rds')
    formula_str = '~ lesion_type + sex + age_norm + pmi_cat'
    subset_spec = list(matter = 'GM')
    method_spec = list(method = 'edgeR', treat = TRUE)
    padj_cut    = 0.05
    fgsea_cut   = 0.05
    filter      = 'both'
    min_cells   = 10
    output_fs   = .make_output_fs(model_dir, run_tag)

  } else if (run_tag == 'run03') {
    # specify where soup and pseudobulk are located
    pb_soup_f   = file.path(soup_dir, 'pb_soup_broad_maximum_2021-06-01.rds')
    soup_cut    = 0.1

    # specify what goes into muscat run
    cluster_var = 'type_broad'
    pb_f        = file.path(soup_dir, 'pb_sum_broad_2021-06-01.rds')
    formula_str = '~ lesion_type + sex + age_norm + pmi_cat'
    subset_spec = list(matter = 'WM')
    method_spec = list(method = 'limma-random', treat = TRUE, 
      random_var = 'patient_id')
    padj_cut    = 0.1
    filter      = 'both'
    min_cells   = 10
    output_fs   = .make_output_fs(model_dir, run_tag)
  
  } else if (run_tag == 'run04') {
    # specify where soup and pseudobulk are located
    pb_soup_f   = file.path(soup_dir, 'pb_soup_broad_maximum_2021-06-01.rds')
    soup_cut    = 0.1

    # specify what goes into muscat run
    cluster_var = 'type_broad'
    pb_f        = file.path(soup_dir, 'pb_sum_broad_2021-06-01.rds')
    formula_str = '~ lesion_type + sex + age_norm + pmi_cat'
    subset_spec = list(matter = 'GM')
    method_spec = list(method = 'limma-random', treat = TRUE, 
      random_var = 'patient_id')
    padj_cut    = 0.1
    filter      = 'both'
    min_cells   = 10
    output_fs   = .make_output_fs(model_dir, run_tag)

  } else if (run_tag == 'run05') {
    # specify where soup and pseudobulk are located
    pb_soup_f   = file.path(soup_dir, 'pb_soup_broad_maximum_no_patient_2021-06-01.rds')
    soup_cut    = 0.1

    # specify what goes into muscat run
    cluster_var = 'type_broad'
    pb_f        = file.path(soup_dir, 'pb_sum_broad_no_patient_2021-06-01.rds')
    formula_str = '~ lesion_type + sex + age_norm + pmi_cat'
    subset_spec = list(matter = 'WM')
    method_spec = list(method = 'edgeR', treat = TRUE)
    padj_cut    = 0.05
    fgsea_cut   = 0.05
    filter      = 'both'
    min_cells   = 10
    output_fs   = .make_output_fs(model_dir, run_tag)

  } else if (run_tag == 'run06') {
    # specify where soup and pseudobulk are located
    pb_soup_f   = file.path(soup_dir, 'pb_soup_broad_maximum_no_patient_2021-06-01.rds')
    soup_cut    = 0.1

    # specify what goes into muscat run
    cluster_var = 'type_broad'
    pb_f        = file.path(soup_dir, 'pb_sum_broad_no_patient_2021-06-01.rds')
    formula_str = '~ lesion_type + sex + age_norm + pmi_cat'
    subset_spec = list(matter = 'GM')
    method_spec = list(method = 'edgeR', treat = TRUE)
    padj_cut    = 0.05
    fgsea_cut   = 0.05
    filter      = 'both'
    min_cells   = 10
    output_fs   = .make_output_fs(model_dir, run_tag)

  } else if (run_tag == 'run07') {
    # specify where soup and pseudobulk are located
    pb_soup_f   = file.path(soup_dir, 'pb_soup_broad_maximum_2021-06-01.rds')
    soup_cut    = 0.1

    # specify what goes into muscat run
    cluster_var = 'type_broad'
    pb_f        = file.path(soup_dir, 'pb_sum_broad_2021-06-01.rds')
    formula_str = '~ age_cat + sex + pmi_cat'
    subset_spec = list(lesion_type = c('NAWM', 'AL', 'CAL', 'CIL', 'RL'))
    method_spec = list(method = 'glmmtmb-nb', random_var = 'patient_id', 
      fc_cut = log(2))
    padj_cut    = 0.05
    fgsea_cut   = 0.05
    filter      = 'both'
    min_cells   = 10
    output_fs   = .make_output_fs(model_dir, run_tag)

  } else if (run_tag == 'run08') {
    # specify where soup and pseudobulk are located
    pb_soup_f   = file.path(soup_dir, 'pb_soup_broad_maximum_2021-06-01.rds')
    soup_cut    = 0.1

    # specify what goes into muscat run
    cluster_var = 'type_broad'
    pb_f        = file.path(soup_dir, 'pb_sum_broad_2021-06-01.rds')
    formula_str = '~ age_cat + sex + pmi_cat'
    subset_spec = list(lesion_type = c('NAGM', 'GML'))
    method_spec = list(method = 'glmmtmb-nb', random_var = 'patient_id', 
      fc_cut = log(2))
    padj_cut    = 0.05
    fgsea_cut   = 0.05
    filter      = 'both'
    min_cells   = 10
    output_fs   = .make_output_fs(model_dir, run_tag)

  } else if (run_tag == 'run09') {
    # specify where soup and pseudobulk are located
    pb_soup_f   = file.path(soup_dir, 'pb_soup_broad_maximum_2021-10-11.rds')
    soup_cut    = 0.1

    # specify what goes into muscat run
    cluster_var = 'type_broad'
    pb_f        = file.path(soup_dir, 'pb_sum_broad_2021-10-11.rds')
    formula_str = '~ lesion_type + sex + age_scale + pmi_cat'
    subset_spec = list(matter = c('WM'))
    method_spec = list(method = 'glmmtmb-nb', random_var = 'subject_id')
    padj_cut    = 0.05
    fgsea_cut   = 0.1
    fc_cut      = log(1.5)
    cpm_cut     = 1
    filter      = 'both'
    min_cells   = 10
    output_fs   = .make_output_fs(model_dir, run_tag)

  } else if (run_tag == 'run10') {
    # specify where soup and pseudobulk are located
    pb_soup_f   = file.path(soup_dir, 'pb_soup_broad_maximum_2021-10-11.rds')
    soup_cut    = 0.1

    # specify what goes into muscat run
    cluster_var = 'type_broad'
    pb_f        = file.path(soup_dir, 'pb_sum_broad_2021-10-11.rds')
    formula_str = '~ lesion_type + sex + age_scale + pmi_cat2'
    subset_spec = list(matter = c('GM'))
    method_spec = list(method = 'glmmtmb-nb', random_var = 'subject_id')
    padj_cut    = 0.05
    fgsea_cut   = 0.1
    fc_cut      = log(1.5)
    cpm_cut     = 1
    filter      = 'both'
    min_cells   = 10
    output_fs   = .make_output_fs(model_dir, run_tag)

  } else if (run_tag == 'run11') {
    # specify where soup and pseudobulk are located
    pb_soup_f   = file.path(soup_dir, 'pb_soup_fine_maximum_2021-10-11.rds')
    soup_cut    = 0.1

    # specify what goes into muscat run
    cluster_var = 'type_fine'
    pb_f        = file.path(soup_dir, 'pb_sum_fine_2021-10-11.rds')
    formula_str = '~ lesion_type + sex + age_scale + pmi_cat'
    subset_spec = list(matter = c('WM'))
    method_spec = list(method = 'glmmtmb-nb', random_var = 'subject_id')
    padj_cut    = 0.05
    fgsea_cut   = 0.1
    fc_cut      = log(1.5)
    cpm_cut     = 1
    filter      = 'both'
    min_cells   = 10
    output_fs   = .make_output_fs(model_dir, run_tag)

  } else if (run_tag == 'run12') {
    # specify where soup and pseudobulk are located
    pb_soup_f   = file.path(soup_dir, 'pb_soup_fine_maximum_2021-10-11.rds')
    soup_cut    = 0.1

    # specify what goes into muscat run
    cluster_var = 'type_fine'
    pb_f        = file.path(soup_dir, 'pb_sum_fine_2021-10-11.rds')
    formula_str = '~ lesion_type + sex + age_scale + pmi_cat2'
    subset_spec = list(matter = c('GM'))
    method_spec = list(method = 'glmmtmb-nb', random_var = 'subject_id')
    padj_cut    = 0.05
    fgsea_cut   = 0.1
    fc_cut      = log(1.5)
    cpm_cut     = 1
    filter      = 'both'
    min_cells   = 10
    output_fs   = .make_output_fs(model_dir, run_tag)

  } else if (run_tag == 'run13') {
    # specify where soup and pseudobulk are located
    pb_soup_f   = file.path(soup_dir, 'pb_soup_broad_maximum_2021-06-01.rds')
    soup_cut    = 0.1

    # specify what goes into muscat run
    cluster_var = 'type_broad'
    pb_f        = file.path(soup_dir, 'pb_sum_broad_2021-06-01.rds')
    formula_str = '~ age_cat + sex + pmi_cat'
    subset_spec = list(lesion_type = c('RL'))
    method_spec = list(method = 'glmmtmb-nb', random_var = 'patient_id')
    padj_cut    = 0.05
    fgsea_cut   = 0.05
    fc_cut      = log(2)
    filter      = 'both'
    min_cells   = 10
    output_fs   = .make_output_fs(model_dir, run_tag)

  } else if (run_tag == 'run14') {
    # specify where soup and pseudobulk are located
    pb_soup_f   = file.path(soup_dir, 'pb_soup_broad_maximum_2021-06-01.rds')
    soup_cut    = 0.1

    # specify what goes into muscat run
    cluster_var = 'type_broad'
    pb_f        = file.path(soup_dir, 'pb_sum_broad_2021-06-01.rds')
    formula_str = '~ lesion_type + sex + age_norm + pmi_cat2'
    subset_spec = list(matter = 'GM')
    method_spec = list(method = 'edgeR', treat = TRUE)
    padj_cut    = 0.05
    fgsea_cut   = 0.05
    filter      = 'both'
    min_cells   = 10
    output_fs   = .make_output_fs(model_dir, run_tag)

  } else if (run_tag == 'run15') {
    # specify where soup and pseudobulk are located
    pb_soup_f   = file.path(soup_dir, 'pb_soup_broad_maximum_2021-06-01.rds')
    soup_cut    = 0.1

    # specify what goes into muscat run
    cluster_var = 'type_broad'
    pb_f        = file.path(soup_dir, 'pb_sum_broad_hack_2021-07-12.rds')
    formula_str = '~ ms_status + sex + ms_status:sex + age_norm + ms_status:age_norm + pmi_cat'
    subset_spec = list(lesion_type = c('WM', 'AL', 'CAL'))
    method_spec = list(method = 'glmmtmb-nb', random_var = 'patient_id')
    padj_cut    = 0.05
    fgsea_cut   = 0.05
    fc_cut      = 0
    filter      = 'both'
    min_cells   = 10
    output_fs   = .make_output_fs(model_dir, run_tag)

  } else if (run_tag == 'run16') {
    # specify where soup and pseudobulk are located
    pb_soup_f   = file.path(soup_dir, 'pb_soup_broad_maximum_2021-06-01.rds')
    soup_cut    = 0.1

    # specify what goes into muscat run
    cluster_var = 'type_broad'
    pb_f        = file.path(soup_dir, 'pb_sum_broad_hack_2021-07-12.rds')
    formula_str = '~ ms_status + sex + ms_status:sex + age_norm + ms_status:age_norm + pmi_cat'
    subset_spec = list(matter = c('GM'))
    method_spec = list(method = 'glmmtmb-nb', random_var = 'patient_id')
    padj_cut    = 0.05
    fgsea_cut   = 0.05
    fc_cut      = 0
    filter      = 'both'
    min_cells   = 10
    output_fs   = .make_output_fs(model_dir, run_tag)

  } else if (run_tag == 'run17') {
    # specify where soup and pseudobulk are located
    pb_soup_f   = file.path(soup_dir, 'pb_soup_broad_maximum_2021-06-01.rds')
    soup_cut    = 0.1

    # specify what goes into muscat run
    cluster_var = 'type_broad'
    pb_f        = file.path(soup_dir, 'pb_sum_broad_hack_2021-07-12.rds')
    formula_str = '~ sex + age_norm + pmi_cat'
    subset_spec = list(matter = c('WM'))
    method_spec = list(method = 'glmmtmb-nb', random_var = 'patient_id')
    padj_cut    = 0.05
    fgsea_cut   = 0.05
    fc_cut      = 0
    filter      = 'both'
    min_cells   = 10
    output_fs   = .make_output_fs(model_dir, run_tag)

  } else if (run_tag == 'run18') {
    # specify where soup and pseudobulk are located
    pb_soup_f   = file.path(soup_dir, 'pb_soup_broad_maximum_2021-06-01.rds')
    soup_cut    = 0.1

    # specify what goes into muscat run
    cluster_var = 'type_broad'
    pb_f        = file.path(soup_dir, 'pb_sum_broad_hack_2021-07-12.rds')
    formula_str = '~ sex + age_norm + pmi_cat'
    subset_spec = list(matter = c('GM'))
    method_spec = list(method = 'glmmtmb-nb', random_var = 'patient_id')
    padj_cut    = 0.05
    fgsea_cut   = 0.05
    fc_cut      = 0
    filter      = 'both'
    min_cells   = 10
    output_fs   = .make_output_fs(model_dir, run_tag)

  } else if (run_tag == 'run19') {
    # specify where soup and pseudobulk are located
    pb_soup_f   = file.path(soup_dir, 'pb_soup_broad_maximum_2021-10-11.rds')
    soup_cut    = 0.1

    # specify what goes into muscat run
    cluster_var = 'type_broad'
    pb_f        = file.path(soup_dir, 'pb_sum_broad_2021-10-11.rds')
    formula_str = '~ lesion_type + sex + age_scale + pmi_cat2'
    subset_spec = list(lesion_type = c('WM', 'GM'))
    method_spec = list(method = 'edgeR', treat = TRUE)
    padj_cut    = 0.05
    fgsea_cut   = 0.1
    fc_cut      = 0
    cpm_cut     = 1
    filter      = 'both'
    min_cells   = 10
    output_fs   = .make_output_fs(model_dir, run_tag)

  } else if (run_tag == 'run20') {
    # specify where soup and pseudobulk are located
    pb_soup_f   = file.path(soup_dir, 'pb_soup_fine_maximum_2021-10-11.rds')
    soup_cut    = 0.1

    # specify what goes into muscat run
    cluster_var = 'type_fine'
    pb_f        = file.path(soup_dir, 'pb_sum_fine_2021-10-11.rds')
    formula_str = '~ lesion_type + sex + age_scale + pmi_cat2'
    subset_spec = list(lesion_type = c('WM', 'GM'))
    method_spec = list(method = 'edgeR', treat = TRUE)
    padj_cut    = 0.05
    fgsea_cut   = 0.1
    fc_cut      = 0
    cpm_cut     = 1
    filter      = 'both'
    min_cells   = 10
    output_fs   = .make_output_fs(model_dir, run_tag)

  } else if (run_tag == 'run21') {
    # specify where soup and pseudobulk are located
    pb_soup_f   = file.path(soup_dir, 'pb_superclean_soup_broad_maximum_2021-08-01.rds')
    soup_cut    = 0.1

    # specify what goes into muscat run
    cluster_var = 'type_broad'
    pb_f        = file.path(soup_dir, 'pb_superclean_sum_broad_2021-08-01.rds')
    formula_str = '~ lesion_type + sex + age_norm + pmi_cat'
    subset_spec = list(matter = c('WM'))
    method_spec = list(method = 'glmmtmb-nb', random_var = 'patient_id')
    padj_cut    = 0.05
    fgsea_cut   = 0.05
    fc_cut      = log(2)
    filter      = 'both'
    min_cells   = 10
    output_fs   = .make_output_fs(model_dir, run_tag)

  } else if (run_tag == 'run22') {
    # specify where soup and pseudobulk are located
    pb_soup_f   = file.path(soup_dir, 'pb_superclean_soup_broad_maximum_2021-08-01.rds')
    soup_cut    = 0.1

    # specify what goes into muscat run
    cluster_var = 'type_broad'
    pb_f        = file.path(soup_dir, 'pb_superclean_sum_broad_2021-08-01.rds')
    formula_str = '~ lesion_type + sex + age_norm + pmi_cat'
    subset_spec = list(matter = c('GM'))
    method_spec = list(method = 'glmmtmb-nb', random_var = 'patient_id')
    padj_cut    = 0.05
    fgsea_cut   = 0.05
    fc_cut      = log(2)
    filter      = 'both'
    min_cells   = 10
    output_fs   = .make_output_fs(model_dir, run_tag)

  } else if (run_tag == 'run23') {
    # specify where soup and pseudobulk are located
    pb_soup_f   = file.path(soup_dir, 'pb_soup_broad_maximum_2021-10-11.rds')
    soup_cut    = 0.1

    # specify what goes into muscat run
    cluster_var = 'type_broad'
    pb_f        = file.path(soup_dir, 'pb_gm_w_pcs_sum_broad_2021-11-12.rds')
    formula_str = '~ lesion_type + sex + age_scale + pmi_cat2 + ctrl_PC01 + ctrl_PC02 + ctrl_PC03 + ctrl_PC04'
    subset_spec = list(matter = c('GM'))
    method_spec = list(method = 'glmmtmb-nb', random_var = 'subject_id')
    padj_cut    = 0.05
    fgsea_cut   = 0.1
    fc_cut      = log(1.5)
    cpm_cut     = 1
    filter      = 'both'
    min_cells   = 10
    output_fs   = .make_output_fs(model_dir, run_tag)

  } else if (run_tag == 'run24') {
    # specify where soup and pseudobulk are located
    pb_soup_f   = file.path(soup_dir, 'pb_soup_fine_maximum_2021-10-11.rds')
    soup_cut    = 0.1

    # specify what goes into muscat run
    cluster_var = 'type_fine'
    pb_f        = file.path(soup_dir, 'pb_gm_w_pcs_sum_fine_2021-11-12.rds')
    formula_str = '~ lesion_type + sex + age_scale + pmi_cat2 + ctrl_PC01 + ctrl_PC02 + ctrl_PC03 + ctrl_PC04'
    subset_spec = list(matter = c('GM'))
    method_spec = list(method = 'glmmtmb-nb', random_var = 'subject_id')
    padj_cut    = 0.05
    fgsea_cut   = 0.1
    fc_cut      = log(1.5)
    cpm_cut     = 1
    filter      = 'both'
    min_cells   = 10
    output_fs   = .make_output_fs(model_dir, run_tag)

  } else if (run_tag == 'run25') {
    # specify where soup and pseudobulk are located
    pb_soup_f   = file.path(soup_dir, 'pb_soup_broad_maximum_2021-10-11.rds')
    soup_cut    = 0.1

    # specify what goes into muscat run
    cluster_var = 'type_broad'
    pb_f        = file.path(soup_dir, 'pb_sum_broad_2021-10-11.rds')
    formula_str = '~ matter + sex + age_scale + pmi_cat'
    subset_spec = list(diagnosis = c('SPMS', 'PPMS', 'RRMS'))
    method_spec = list(method = 'glmmtmb-nb', random_var = 'subject_id')
    padj_cut    = 0.05
    fgsea_cut   = 0.1
    fc_cut      = log(1.5)
    cpm_cut     = 1
    filter      = 'both'
    min_cells   = 10
    output_fs   = .make_output_fs(model_dir, run_tag)
  }

  # actually do run
  run_muscat_and_gsea(
    run_tag     = run_tag,
    pb_f        = pb_f,
    subset_spec = subset_spec,
    pb_soup_f   = pb_soup_f,
    labels_f    = labels_f,
    cluster_var = cluster_var,
    labels_dt   = labels_dt,
    formula_str = formula_str,
    padj_cut    = padj_cut,
    fgsea_cut   = fgsea_cut,
    fc_cut      = fc_cut,
    cpm_cut     = cpm_cut,
    min_cells   = min_cells,
    method_spec = method_spec,
    filter      = filter,
    soup_cut    = soup_cut,
    paths_list  = paths_list,
    n_cores     = n_cores,
    output_fs   = output_fs
  )
}

.make_output_fs <- function(model_dir, run_tag) {
  output_fs   = list(
      res_f       = sprintf('%s/muscat_res_%s_%s.rds', model_dir, run_tag, '%s'),
      res_raw_f   = sprintf('%s/muscat_res_raw_%s_%s.txt.gz', model_dir, run_tag, '%s'),
      res_dt_f    = sprintf('%s/muscat_res_dt_%s_%s.txt.gz', model_dir, run_tag, '%s'),
      ranef_dt_f  = sprintf('%s/muscat_ranef_dt_%s_%s.txt.gz', model_dir, run_tag, '%s'),
      checks_f    = sprintf('%s/muscat_checks_dt_%s_%s.txt.gz', model_dir, run_tag, '%s'),
      goodness_f  = sprintf('%s/muscat_goodness_dt_%s_%s.txt.gz', model_dir, run_tag, '%s'),
      fgsea_pat   = sprintf('%s/fgsea_dt_%s_%s_%s.txt.gz', model_dir, run_tag, '%s', '%s'),
      mds_dist_ls_f   = sprintf('%s/mds_dist_ls_%s_%s.rds', model_dir, run_tag, '%s'),
      mds_dist_f  = sprintf('%s/mds_dist_%s_%s.rds', model_dir, run_tag, '%s'),
      mds_all_f   = sprintf('%s/mds_all_dt_%s_%s.txt.gz', model_dir, run_tag, '%s'),
      mds_sep_f   = sprintf('%s/mds_sep_dt_%s_%s.txt.gz', model_dir, run_tag, '%s'),
      xl_f        = sprintf('%s/muscat_%s_%s.xlsx', model_dir, run_tag, '%s'),
      xl_wide_f   = sprintf('%s/muscat_wide_%s_%s.xlsx', model_dir, run_tag, '%s'),
      fgsea_xl_f  = sprintf('%s/muscat_GSEA_%s_%s.xlsx', model_dir, run_tag, '%s'),
      params_f    = sprintf('%s/muscat_params_%s_%s.rds', model_dir, run_tag, '%s'),
      log_f       = sprintf('%s/muscat_log_%s_%s.txt', model_dir, run_tag, '%s')
    )
}

render_muscat_results <- function(run_tags, time_stamps, slim = TRUE, 
  n_top = 10, n_top_paths = 60, max_fc = log(16)) {
  # some checks
  assert_that(all(run_tags %in% names(run_descs)))
  n_stamps    = length(time_stamps)
  assert_that( n_stamps == 1 |
    length(run_tags) == length(time_stamps))
  if (n_stamps == 1)
    time_stamps   = rep(time_stamps, length(run_tags))

  for (i in seq_along(run_tags)) {
    run_tag     = run_tags[[i]]
    time_stamp  = time_stamps[[i]]
    .render_one_muscat(run_tag, time_stamp, slim, n_top, n_top_paths, max_fc)
  }
}

.render_one_muscat <- function(run_tag, time_stamp, slim, 
  n_top, n_top_paths, max_fc) {
  # define files to load
  model_dir = file.path('output/ms10_muscat', run_tag)
  muscat_f  = '%s/muscat_res_dt_%s_%s.txt.gz' %>%
    sprintf(model_dir, run_tag, time_stamp)
  res_raw_f = '%s/muscat_res_raw_%s_%s.txt.gz' %>% 
    sprintf(model_dir, run_tag, time_stamp)
  checks_f  = '%s/muscat_checks_dt_%s_%s.txt.gz' %>% 
    sprintf(model_dir, run_tag, time_stamp)
  anova_f     = '%s/muscat_goodness_dt_%s_%s.txt.gz' %>%
    sprintf(model_dir, run_tag, time_stamp)
  params_f  = '%s/muscat_params_%s_%s.rds' %>%
    sprintf(model_dir, run_tag, time_stamp)
  mds_mat_f = '%s/mds_dist_%s_%s.rds' %>%
    sprintf(model_dir, run_tag, time_stamp)
  mds_all_f = '%s/mds_all_dt_%s_%s.txt.gz' %>%
    sprintf(model_dir, run_tag, time_stamp)
  mds_sep_f = '%s/mds_sep_dt_%s_%s.txt.gz' %>%
    sprintf(model_dir, run_tag, time_stamp)
  fgsea_pat = '%s/fgsea_dt_%s_%s_%s.txt.gz' %>%
    sprintf(model_dir, run_tag, '%s', time_stamp)
  fgsea_fs  = sapply(names(paths_list), 
    function(n) sprintf(fgsea_pat, n))

  # check all exist
  assert_that(
    file.exists(muscat_f),
    file.exists(params_f)
    # all(file.exists(fgsea_fs))
  )
  mds_flag    = all(file.exists(c(mds_mat_f, mds_all_f), mds_sep_f))
  if (!mds_flag)
    warning('mds files not complete; not plotting')

  message('rendering report for ', run_tag, ', ', time_stamp, sep = '')
  message('  loading results')

  # load them
  params      = params_f %>% readRDS
  if (is.null(params$fc_cut))
    params$fc_cut   = 1

  # load_muscat_inputs
  pb          = readRDS(params$pb_f) %>% 
    .subset_pb(params$subset_spec)
  labels_dt   = .load_labels_dt(labels_f, params$cluster_var)
  magma_dt    = .load_magma_dt(magma_f, pb)
  tfs_dt      = .load_tfs_dt(tfs_f, pb)
  lof_dt      = .load_lof_dt(lof_f, pb)
  coloc_dt    = .load_coloc_dt(coloc_f, pb)

  # get muscat results
  res_all     = muscat_f %>% fread
  res_dt      = .load_muscat_results(res_all, labels_dt, params)
  sigmas_dt   = .load_sigmas_dt(res_raw_f)

  # load anova results
  anova_dt    = .load_anova_dt(anova_f, res_dt) %>%
    .[ is.na(full), full := 1 ]

  # load_mds
  if (mds_flag) {
    mds_dist    = mds_mat_f %>% readRDS
    mds_all_dt  = mds_all_f %>% fread
    mds_sep_dt  = mds_sep_f %>% fread
    if (params$cluster_var == 'type_broad')
      mds_sep_dt[, cluster_id := factor(cluster_id, levels = broad_ord)]
  } else {
    mds_all_dt  = NULL
    mds_sep_dt  = NULL
  }

  # load_fgsea
  message('  loading GSEA outputs')
  fgsea_list  = .load_fgseas_list(fgsea_fs, labels_dt)

  # prep for stacked bars
  signif_dt   = res_dt[ updown_soup != 'insignif' & !is.na(p_adj.soup) ]
  assert_that(all(abs(signif_dt$logFC) >= params$fc_cut))
  uniques_dt  = .calc_uniques_dt(signif_dt, params)
  stacked_dt  = .calc_stacked_dt(uniques_dt)

  # restrict to significant genes only
  message('  getting top genes')
  top_dt      = .calc_top_genes(signif_dt, res_dt, uniques_dt, 
    magma_dt, tfs_dt, lof_dt, coloc_dt, params$fc_cut, n_top)
  top_gwas_dt = .calc_top_gwas_dt(res_dt, uniques_dt, magma_dt, 
    tfs_dt, lof_dt, coloc_dt, fc_cut = NULL, magma_cut = 0.05)

  # get random effects
  ranef_dt_f  = sprintf('%s/muscat_ranef_dt_%s_%s.txt.gz', 
    model_dir, run_tag, time_stamp)
  if (file.exists(ranef_dt_f)) {
    message('  getting random effects')
    ranef_dt    = .load_ranef_dt(ranef_dt_f, labels_dt, pb)
    soup_dt     = res_dt[, .(cluster_id, gene_id, mean_soup)] %>% unique
  } else {
    ranef_dt    = NULL
  }

  # get diagnostic values
  if (file.exists(checks_f)) {
    message('  getting R2 values')
    checks_dt   = .load_checks_dt(checks_f, res_dt)
  } else {
    checks_dt   = NULL
  }
  # define where to save report
  report_f    = sprintf('ms10_muscat_%s_%s', run_tag, time_stamp)
  if (slim)
    report_f    = paste0(report_f, "_slim")
  docs_dir    = 'public/de_reports'

  # delete cache directory
  message('  deleting cache directory')
  if (params$cluster_var == 'type_broad') {
    cache_dir   = 'analysis/ms10_muscat_template_broad_cache'
  } else if (params$cluster_var == 'type_fine') {
    cache_dir   = 'analysis/ms10_muscat_template_fine_cache'
  }
  if (dir.exists(cache_dir))
    unlink(cache_dir, recursive = TRUE)

  # render
  if (params$cluster_var == 'type_broad') {
    if (slim) {
      template_f  = "analysis/ms10_muscat_template_broad_slim.Rmd"
    } else {
      template_f  = "analysis/ms10_muscat_template_broad.Rmd"
    }
  } else if (params$cluster_var == 'type_fine') {
    if (slim) {
      template_f  = "analysis/ms10_muscat_template_fine_slim.Rmd"
    } else {
      template_f  = "analysis/ms10_muscat_template_fine.Rmd"
    }
  }

  message('  rendering report')
  tryCatch({
    render(input = template_f, 
      output_format = workflowr::wflow_html(),
      output_file = report_f, output_dir = docs_dir, 
      output_options = list(self_contained = FALSE, lib_dir = "public/site_libs"),
      output_yaml = 'analysis/_site.yml', 
      clean = TRUE, quiet = FALSE)
  }, error=function(cond) {
    message("Something went wrong with rendering your report :/\n",
      "Here's the original error message:",
      cond)
    return(NA)
  })
}
