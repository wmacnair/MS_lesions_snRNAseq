#!/usr/bin/env python
# python code/ms11_paga_fns.py

print('\nimporting libraries')
import os
import re
import copy
import numpy as np
import pandas as pd
import scanpy as sc
import anndata
import warnings

# from annoy import AnnoyIndex
import scipy.sparse as sp
from collections import OrderedDict

sc.settings.n_jobs    = 8
os.environ["TF_CPP_MIN_LOG_LEVEL"] = "2" 

def run_paga(save_dir, save_tag, date_tag, conos_df, graph_f, group_var, overwrite=False, 
  min_cells = 20):
  # define outputs
  to_save   = {
    'connectivities':       f'{save_dir}/paga_mat_{save_tag}_{date_tag}.txt',
    'connectivities_tree':  f'{save_dir}/paga_tree_{save_tag}_{date_tag}.txt',
    'pos_all':              f'{save_dir}/paga_pos_all_{save_tag}_{date_tag}.txt',
    'pos_olg':              f'{save_dir}/paga_pos_olg_{save_tag}_{date_tag}.txt'
    }

  if all([os.path.isfile(f) for f in to_save.values()]) and not overwrite:
    # print(f'{save_tag} already done!')
    return

  # set directory
  # import pdb; pdb.set_trace()
  print('making adata object')
  with warnings.catch_warnings():
    warnings.filterwarnings("ignore", category = anndata.ImplicitModificationWarning)
    adata     = make_adata(graph_f, conos_df)

  # run PAGA
  if adata.shape[0] < min_cells:
    print('insufficient cells; not running')
    return()

  print('running PAGA on all')
  try:
    with warnings.catch_warnings():
      warnings.filterwarnings("ignore", category = FutureWarning)
      sc.tl.paga(adata, groups = group_var)
      sc.pl.paga(adata, color = group_var, save = '_all_' + save_tag + '_' + date_tag + '.png')
  except IndexError:
    print(f"problem with {save_tag} :(")
    return

  save_pos(adata, group_var, to_save['pos_all'])

  print('running PAGA on oligos + OPCs only')
  olg_idx     = adata.obs['type_broad'].isin(['Oligodendrocytes', 'OPCs / COPs'])
  adata_olg   = copy.deepcopy(adata)[olg_idx, :]
  if adata_olg.shape[0] >= min_cells:
    sc.tl.paga(adata_olg, groups = group_var)
    sc.pl.paga(adata_olg, color = group_var, 
      save = '_olg_' + save_tag + '_' + date_tag + '.png')
    save_pos(adata_olg, group_var, to_save['pos_olg'])
  else:
    print('insufficient oligos; not running for them')

  print('saving outputs')
  names_df  = pd.DataFrame(adata.obs[group_var].cat.categories, 
    columns = [group_var])
  sizes_df  = pd.DataFrame(adata.uns[group_var + '_sizes'],
    columns = ['n_cells'])
  for s in ['connectivities', 'connectivities_tree']:
    obj   = adata.uns['paga'][s]
    if sp.issparse(obj):
      obj   = obj.todense()
    tmp_df  = pd.concat([
      names_df,
      sizes_df,
      pd.DataFrame(obj, columns=names_df[group_var])
      ], axis=1)
    tmp_df.to_csv(to_save[s])


def make_adata(graph_f, conos_df):
  # get clusters
  # print('  loading subgraph')
  graph_df    = pd.read_csv(graph_f)\
    .rename(columns={'from': 'cell_i', 'to': 'cell_j'})

  # print('  adding edges in other direction')
  graph_df  = pd.concat(
    [graph_df,
    graph_df.rename(columns={'cell_i': 'cell_j', 'cell_j': 'cell_i'})],
    axis = 0)

  # print('  defining set of cells')
  sel_ids   = np.array(list(set(graph_df['cell_i'].tolist())))
  n_cells   = len(sel_ids)
  cells_lu  = pd.DataFrame({'cell_id':sel_ids, 'index':np.arange(n_cells)})

  # print('  subsetting conos')
  conos_sub = conos_df[conos_df.cell_id.isin(sel_ids)]
  conos_sub  = conos_sub.merge(cells_lu, how = 'left', on = 'cell_id')\
    .set_index('index').sort_index()

  # print('  adding index to graph')
  graph_df  = graph_df.merge(
    cells_lu.rename(columns={'cell_id': 'cell_i', 'index': 'index_i'}), 
    how='left', on='cell_i').merge(
    cells_lu.rename(columns={'cell_id': 'cell_j', 'index': 'index_j'}), 
    how='left', on='cell_j')

  # make dummy expression, variable dfs
  dummy_exp     = pd.DataFrame( {'var0': np.repeat(0, n_cells)} )
  dummy_var     = pd.DataFrame( {'marker': ['var0']} )
  dummy_var.index = dummy_var['marker']

  # make fake adata
  adata  = anndata.AnnData(
    X     = dummy_exp,
    obs   = conos_sub,
    var   = dummy_var
  )

  # make connectivities graph
  # print('  making distance matrices')
  connects  = sp.csr_matrix(
    (graph_df.weight, (graph_df.index_i, graph_df.index_j)), 
    shape=(n_cells, n_cells))
  distances   = sp.csr_matrix(
    (1-graph_df.weight, (graph_df.index_i, graph_df.index_j)), 
    shape=(n_cells, n_cells))

  # calc fake neighbors
  # print('  calculating neighbourhood distances')
  sc.pp.neighbors(adata)

  # add real values neighbors
  adata.obsp['connects']  = connects
  adata.obsp['distances'] = distances

  return adata


def save_pos(a, group_var, f):
  names_df  = pd.DataFrame(a.obs[group_var].cat.categories, 
    columns = [group_var])
  obj       = a.uns['paga']['pos']
  tmp_df    = pd.concat([
    names_df,
    pd.DataFrame(obj, columns=['paga1', 'paga2'])
    ], axis=1)
  tmp_df.to_csv(f)


if __name__ == '__main__':
  # conos_f     = 'output/ms11_paga/conos_tidy_2022-01-07.txt.gz'
  conos_f     = 'output/ms11_paga/conos_tidy_recalc_2021-08-01.txt.gz'
  conos_df    = pd.read_csv(conos_f)

  save_dir    = 'output/ms11_paga'
  # date_tag    = '2022-01-04_clean'
  # date_tag    = '2022-01-07'
  date_tag    = 'recalc_2021-08-01'

  # sample_ls   = conos_df.sample_id.unique().tolist()
  group_var   = 'type_fine'
  subset_var  = ''
  # subset_var  = 'lesion_'
  # for s in ['GM', 'NAGM', 'GML']:
  # import pdb; pdb.set_trace()
  for s in ['healthy_GM', 'MS_GM']:
    # check if already done
    save_tag    = f'{subset_var}{s}'
    last_f      = f'{save_dir}/paga_mat_{save_tag}_{date_tag}.txt'
    if os.path.exists(last_f):
      continue

    # if not, run paga
    print(s)
    graph_f     = f'{save_dir}/subgraph_{save_tag}_{date_tag}.txt.gz'

    run_paga(save_dir, save_tag, date_tag, conos_df, graph_f, group_var, overwrite=False)

